// explained here: 
// http://www.codebelt.com/javascript/install-grunt-js-on-windows/


module.exports = function( grunt ){
     // Project configuration.
    grunt.initConfig({
         //Read the package.json (optional)
        pkg: grunt.file.readJSON('package.json'),
        // Metadata.
        meta: {
            basePath: '../',
            srcPath: '../src/',
            deployPath: '../deploy/'
        },
 
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
                '* Copyright (c) <%= grunt.template.today("yyyy") %> ',
 

        clean: ['<%= meta.deployPath %>'],


        // Task configuration.
        concat: {
            options: {
                stripBanners: true
            },
            dist: {
                src: [
                    '<%= meta.srcPath %>/ximpel/polyfills.js', 
                    '<%= meta.srcPath %>/ximpel/ximpel.js', 
                    '<%= meta.srcPath %>/ximpel/View.js', 
                    '<%= meta.srcPath %>/ximpel/Models.js', 
                    '<%= meta.srcPath %>/ximpel/XimpelApp.js', 
                    '<%= meta.srcPath %>/ximpel/Player.js', 
                    '<%= meta.srcPath %>/ximpel/Parser.js', 
                    '<%= meta.srcPath %>/ximpel/MediaPlayer.js', 
                    '<%= meta.srcPath %>/ximpel/QuestionManager.js', 
                    '<%= meta.srcPath %>/ximpel/SequencePlayer.js', 
                    '<%= meta.srcPath %>/ximpel/MediaType.js', 
                    '<%= meta.srcPath %>/ximpel/MediaTypeRegistration.js', 
                    '<%= meta.srcPath %>/ximpel/PubSub.js', 

                    '<%= meta.srcPath %>/ximpel/XimpelAppView.js', 
                    '<%= meta.srcPath %>/ximpel/OverlayView.js', 
                    '<%= meta.srcPath %>/ximpel/QuestionView.js', 

                    '<%= meta.srcPath %>/ximpel/Video.js',
                    '<%= meta.srcPath %>/ximpel/Image.js',
                    '<%= meta.srcPath %>/ximpel/Audio.js',
                    '<%= meta.srcPath %>/ximpel/YouTube.js',
                    '<%= meta.srcPath %>/ximpel/WebPage.js',
                    '<%= meta.srcPath %>/ximpel/Message.js'

                ],
                dest: '<%= meta.deployPath %>/ximpel/ximpel.js'
            }
        },

        copy: {
            main: {
                files: [
                    { expand: true, cwd: '<%= meta.srcPath %>/ximpel', src: ['ximpel.css'], dest: '<%= meta.deployPath %>/ximpel/'},
                    { expand: true, cwd: '<%= meta.srcPath %>/ximpel', src: ['images/*'], dest: '<%= meta.deployPath %>/ximpel/'},
                    { expand: true, cwd: '<%= meta.srcPath %>/php', src: ['*'], dest: '<%= meta.deployPath %>/php/'},
                    { expand: true, cwd: '<%= meta.srcPath %>/css', src: ['*'], dest: '<%= meta.deployPath %>/css/'},
                    { expand: true, cwd: '<%= meta.srcPath %>/db', src: ['*'], dest: '<%= meta.deployPath %>/db/'},
                    { expand: true, cwd: '<%= meta.srcPath %>/js', src: ['*'], dest: '<%= meta.deployPath %>/js/'},
                    { expand: true, cwd: '<%= meta.srcPath %>/media', src: ['*'], dest: '<%= meta.deployPath %>/media/'},
                    { expand: true, cwd: '<%= meta.srcPath %>', src: ['*'], dest: '<%= meta.deployPath %>'}
                ],
            },
        }


    });
 
    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');

 
    // Default task
    grunt.registerTask('default', ['clean','concat','copy']);
 
};