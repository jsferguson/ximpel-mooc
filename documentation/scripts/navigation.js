var html = 
   ['<div id="mainMenuContainer">',
    	'<div id="mainMenu">',
    		'<h3>Architecture</h3>',
			'<ul>',
				'<li><a href="architecture_overview.htm">Overview</a></li>',
				'<li><a href="architecture_resolution.htm">Resolution</a></li>',
	   			'<li><a href="architecture_modifying_interface.htm">Customizing</a></li>',
	   			'<li><a href="architecture_database.htm">Database</a></li>',
			'</ul>',    		
			'',
			'<h3>XIMPEL Introduction</h3>',
			'<ul>',
				'<li><a href="playlist_introduction.htm">Introduction</a></li>',
				'<li><a href="playlist_subjects.htm">Subjects</a></li>',
				'<hr />',
				'<li><a href="playlist_media_types.htm">Media Types</a></li>',
				'<li><a href="playlist_video.htm">Video</a></li>',
				'<li><a href="playlist_audio.htm">Audio</a></li>',
				'<li><a href="playlist_image.htm">Image</a></li>',
				'<li><a href="playlist_youtube.htm">Youtube</a></li>',
	   			'<li><a href="playlist_linkpage.htm">LinkPage</a></li>',
	   			'<li><a href="playlist_messages.htm">Messages</a></li>',
				'<hr />',
				'<li><a href="playlist_overlays.htm">Overlays</a></li>',
				'<li><a href="playlist_questions.htm">Questions</a></li>',
				'<li><a href="playlist_variables.htm">Variables / Scores</a></li>',
	   			'<li><a href="playlist_dynamic_branches.htm">Dynamic Branches</a></li>',
	   			'<li><a href="playlist_assessment_quizzes.htm">Assessment Quizzes</a></li>',
				'<hr />',
				'<li><a href="playlist_config.htm">Config</a></li>',
			'</ul>',
			'',
			'<h3>Getting started</h3>',
			'<ul>',
				'<li><a href="getting_started_steps.htm">Step by step guide</a></li>',
				'<li><a href="getting_started_configuration_options.htm">XimpelApp options</a></li>',
			'</ul>',
			'',
			'<h3>Custom Media Types</h3>',
			'<ul>',
				'<li><a href="media_types_construction.htm">Building a media type</a></li>',
				'<li><a href="media_types_extra.htm">Extra</a></li>',
			'</ul>',
		   '<h3>XIMPEL users</h3>',
		   '<ul>',
		   '<li><a href="users_management_console.htm">Managing Users</a></li>',
		   '</ul>',
		'</div>',
	'</div>',
   ].join('\n');

document.write( html );

				
			


			
			
				
				
			
		
	