
<?php
/*
 *  This includes the checkSessionID function, which checks if the current viewer has logged on, by checking
 *  if the sessionID exists in the database.
 */
include('./php/validate_user.php');
function getDB(){

    $file_db = new PDO('sqlite:./db/ximpel.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $file_db;
}

// if the uses does not have a valid sessionID, redirect to the logon page
if (!checkSessionID()){

    header('Location: user_logon.php');
}

?>


<!DOCTYPE html>
<html>
<head>
    <title>FRISSR Demo</title>
    <link rel="stylesheet" href="./ximpel/ximpel.css" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="./ximpel/polyfills.js"></script>
    <script type="text/javascript" src="./ximpel/ximpel.js"></script>
    <script type="text/javascript" src="./ximpel/View.js"></script>
    <script type="text/javascript" src="./ximpel/Models.js"></script>
    <script type="text/javascript" src="./ximpel/XimpelApp.js"></script>
    <script type="text/javascript" src="./ximpel/Player.js"></script>
    <script type="text/javascript" src="./ximpel/Parser.js"></script>
    <script type="text/javascript" src="./ximpel/MediaPlayer.js"></script>
    <script type="text/javascript" src="./ximpel/QuestionManager.js"></script>
    <script type="text/javascript" src="./ximpel/SequencePlayer.js"></script>
    <script type="text/javascript" src="./ximpel/MediaType.js"></script>
    <script type="text/javascript" src="./ximpel/MediaTypeRegistration.js"></script>
    <script type="text/javascript" src="./ximpel/PubSub.js"></script>
    <script type="text/javascript" src="./ximpel/XimpelAppView.js"></script>
    <script type="text/javascript" src="./ximpel/OverlayView.js"></script>
    <script type="text/javascript" src="./ximpel/QuestionView.js"></script>
    <script type="text/javascript" src="./ximpel/Video.js"></script>
    <script type="text/javascript" src="./ximpel/Image.js"></script>
    <script type="text/javascript" src="./ximpel/Audio.js"></script>
    <script type="text/javascript" src="./ximpel/YouTube.js"></script>
    <script type="text/javascript" src="./ximpel/WebPage.js"></script>
    <script type="text/javascript" src="./ximpel/Message.js"></script>



    <!-- NEW SNIPPET -->
    <script type="text/javascript">
        $( document ).ready(function(){
            var myXimpelApp = new ximpel.XimpelApp(
                    'someappid',
                    'frissr.xml',
                    'config.xml',
                    {
                        'appWidth'      : '1680px',
                        'appHeigth'     : '1050px',
                        'loadProfile'   :  'false'
                    }
            );
            myXimpelApp.load();
        });
    </script>
    <!-- NEW SNIPPET -->
</head>
<body>

</body>
</html>