/*
 *  This is a customer media type for creating media items that can present hyperlinks to the viewer.
 *  Links that are clicked are shown in a new tab, so that the user does not navigate away from the XIMPEL application.
 */
ximpel.mediaTypeDefinitions.LinkPage = function( customEl, customAttr, $el, player ){
    this.customElements = customEl;
    this.customAttributes = customAttr;
    this.$parentElement = $el;
    this.player = player;
    // prevent the content from being generated again, if the user briefly pauses the player
    this.hasBeenGenerated = false;
    this.state = 'stopped';
};

// prevent the link click handler from being registered more than once. This applies to the whole XIMEL application
var linkClickRegistered = false;


ximpel.mediaTypeDefinitions.LinkPage.prototype = new ximpel.MediaType();

ximpel.mediaTypeDefinitions.LinkPage.prototype.appendLinks = function(){

    // iterate through all the link elements in the media item
    for (var link in this.customElements){

        // create an <a> tag
        var newLink = $("<a />");
        
        // add the ximpel_links class to the <a> element
        newLink.addClass("ximpel_links");
        
        // add the href attribute and the url
        newLink.attr("href", this.customElements[link].elementAttributes.url);
        
        // make sure the link opens in a new browser tab
        newLink.attr("target", "_blank");
        
        // add the text to the link text (this is what is shown on the screen).
        newLink.text(this.customElements[link].elementAttributes.description);
        
        // set the font-size of the link. If not set, use the value from the CSS-file.
        if(this.customElements[link].elementAttributes.fontSize){

            newLink.css("font-size", this.customElements[link].elementAttributes.fontSize);
        }
        //newLink.css("font-size", this.customElements[link].elementAttributes.fontSize);

        // set the color.  If not set, use the value from the CSS-file.
        if(this.customElements[link].elementAttributes.color){

            newLink.css("color", this.customElements[link].elementAttributes.color);
        }
        // set the background color.  If not set, use the value from the CSS-file.
        if(this.customElements[link].elementAttributes.backgroundColor){

            newLink.css("background-color", this.customElements[link].elementAttributes.backgroundColor);
        }
        //if the playlist specifies a background image, set it in the CSS properties


        //append the link to the container and add a break
        this.$messageSpan.append(newLink);
        this.$messageSpan.append('<br>');
    }
};

ximpel.mediaTypeDefinitions.LinkPage.prototype.mediaPlay = function(){

    // if the content has not yet been generated, generate the content and register the click-handler.
    if (!this.hasBeenGenerated){

        this.generateContent();
        this.hasBeenGenerated = true;
        this.registerLinkClick(this);
    }

    this.state = 'playing';

};

ximpel.mediaTypeDefinitions.LinkPage.prototype.mediaPause = function(){
    this.state = 'paused';
};

/*
 *  If the media-subject is stopped, set generated to false and remove the background image.
 */
ximpel.mediaTypeDefinitions.LinkPage.prototype.mediaStop = function(){
    this.state = 'stopped';
    this.hasBeenGenerated = false;
    this.$parentElement.css('background-image', 'none');
    this.$parentElement.css('background-color', 'black');
    this.$messageSpan.detach();
};

ximpel.mediaTypeDefinitions.LinkPage.prototype.mediaIsPlaying = function(){

    return this.state === 'playing';
};

ximpel.mediaTypeDefinitions.LinkPage.prototype.mediaIsPaused = function(){
    return this.state === 'paused';
};

ximpel.mediaTypeDefinitions.LinkPage.prototype.mediaIsStopped = function(){

    return this.state === 'stopped';
};

// Register the media type with XIMPEL
var r = new ximpel.MediaTypeRegistration('linkpage', ximpel.mediaTypeDefinitions.LinkPage, {
    'allowedAttributes': ['text', 'url', 'description', 'background', 'fontSize'],
    'requiredAttributes': ['text'],
    'allowedChildren': ['link', 'overlay'],
    'requiredChildren': []
} );




ximpel.registerMediaType( r );

/*
 *  This method is called when the LinkPage needs to be created.
 */
ximpel.mediaTypeDefinitions.LinkPage.prototype.generateContent = function () {

    // create a container <div> and assign the id;
    this.$messageSpan = $('<div id="ximpel_link_page"></div>');
    this.$parentElement.append( this.$messageSpan );

    // create  <span> for the title
    var $newTitle = $("<span />");
    $newTitle.addClass('page_title_container');
    
    if(this.customAttributes.fontSize){

        $newTitle.css("font-size", this.customAttributes.fontSize);
    }

    if(this.customAttributes.color){

        $newTitle.css("color", this.customAttributes.color);
    }
    if(this.customAttributes.backgroundColor){

        $newTitle.css("backgroundColor", this.customAttributes.backgroundColor);
    }
    // if a background is specified, assign tot the div-container
    if (this.customAttributes.backgroundImage){

        this.$parentElement.css({

            'background' : 'url('+this.customAttributes.backgroundImage+')',
            'background-size' : '100% 100%'
        });
    }

    
    // add the title
    $newTitle.text(this.customAttributes.text);
    $newTitle.append('<br>');
    this.$messageSpan.append($newTitle);
    this.appendLinks();




};


/*
 *  create a handler for the hyperlinks, so that visited links can be added to the database for the user
 */
ximpel.mediaTypeDefinitions.LinkPage.prototype.registerLinkClick = function(obj){

    if (linkClickRegistered == false){
        linkClickRegistered = true;

        // the click-event needs to be attached to a static element, which is the <body>.
        $('body').on('click', 'a.ximpel_links', function() {

            // create an object that contains the hyperlink details.
            var linkdData = {
                "url"           : this.href,
                "description"   : this.text,
                "command"       : "INSERT_VISITED_LINK"
            };
            // call the method that inserts the link to the database and pass the hyperlink details
            obj.saveClickedLinkToDatabase(linkdData);
        });
    }
};

/*
 *  The method takes an object containing the details regarding the hyperlink that was pressed by the user
 *  and uses AJAX to post it to a PHP-handler.
 */
ximpel.mediaTypeDefinitions.LinkPage.prototype.saveClickedLinkToDatabase = function(linkData){

    $.ajax({
        type         : 'POST',
        url          : './php/profile.php',
        data         : linkData,
        dataType     : 'json',
        encode       : true,
        success     : function (data){

            if (data.success == false){

                ximpel.error("Database insert failed!: " + data.err);
            }
            else {

            }
        },
        error       : function (data){

            ximpel.error("Serious database error: " + JSON.stringify(data));
        }
    });
};
