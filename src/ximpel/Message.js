/**
 * Created by jari on 02/06/16.
 */
ximpel.mediaTypeDefinitions.Message = function( customEl, customAttr, $el, player ){
    this.customElements = customEl;
    this.customAttributes = customAttr;
    this.$parentElement = $el;
    this.player = player;
    this.hasBeenGenerated = false;
    this.state = 'stopped';
};


ximpel.mediaTypeDefinitions.Message.prototype = new ximpel.MediaType();



ximpel.mediaTypeDefinitions.Message.prototype.mediaPlay = function(){

    //prevent the contents from being re-generated, if the page is visited again or if the user briefly pauses
    //the player.
    if (!this.hasBeenGenerated){

        this.generateContent();
        this.hasBeenGenerated = true;
    }
    this.state = 'playing';


};

ximpel.mediaTypeDefinitions.Message.prototype.mediaPause = function(){
    this.state = 'paused';
};

ximpel.mediaTypeDefinitions.Message.prototype.mediaStop = function(){
    this.state = 'stopped';
    this.hasBeenGenerated = false;
    this.$messageSpan.detach();
    this.$parentElement.css('background-image', 'none');
    this.$parentElement.css('background-color', 'black');

};

ximpel.mediaTypeDefinitions.Message.prototype.mediaIsPlaying = function(){

    return this.state === 'playing';
};

ximpel.mediaTypeDefinitions.Message.prototype.mediaIsPaused = function(){
    return this.state === 'paused';
};

ximpel.mediaTypeDefinitions.Message.prototype.mediaIsStopped = function(){

    return this.state === 'stopped';
};

// Register the media type with XIMPEL
var r = new ximpel.MediaTypeRegistration('messages', ximpel.mediaTypeDefinitions.Message, {
    'allowedAttributes': ['text', 'fontSize', 'backgroundColor', 'backgroundImage'],
    'requiredAttributes': [],
    'allowedChildren': ['message'],
    'requiredChildren': []
} );




ximpel.registerMediaType( r );

/*
 *  This method is called when the page needs to be created. It will first create a <div> that will contain
 *  the contents and append it to the parent (player).
 */
ximpel.mediaTypeDefinitions.Message.prototype.generateContent = function(){

    this.$messageSpan = $('<div id="ximpel_message_page"></div>');
    this.$parentElement.append(this.$messageSpan );

    //if the playlist specifies a background image, set it in the CSS properties
    if(this.customAttributes.backgroundImage){

        this.$parentElement.css({

            'background' : 'url('+this.customAttributes.backgroundImage+')',
            'background-size' : '100% 100%'
        });
    }

    //call the method that generates the text messages that are listed in the playlist
    this.generateMessages();

    
};

/*
 *  This method iterates the message elements in the customElements and appends each message to the messageSpan
 *  Although the style is set in the external CSS file (ximpel.css), this can be overridden by values in the playlist
 *  The style-values are:
 *  - color
 *  - fontSize
 *  - background color
 */
ximpel.mediaTypeDefinitions.Message.prototype.generateMessages = function () {
    
    for (var message in this.customElements){
        
        //create a paragraph element
        var newLink = $("<p />");
        
        //add the ximpel_message class to the paragraph
        newLink.addClass("ximpel_message");
        
        if (this.customElements[message].elementAttributes.color){

            newLink.css("color",this.customElements[message].elementAttributes.color);
        }
        if (this.customElements[message].elementAttributes.fontSize){

            newLink.css("font-size",this.customElements[message].elementAttributes.fontSize);
        }

        if (this.customElements[message].elementAttributes.backgroundColor){

            newLink.css("background-color",this.customElements[message].elementAttributes.backgroundColor);
        }
        
        newLink.text(this.customElements[message].elementAttributes.text);

        //append the paragraph item to the messageSpan item
        this.$messageSpan.append(newLink);
    }
};

