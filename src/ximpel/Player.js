// Player()
// The ximpel.Player object is the object that manages the actual playing of the presentation. 
// The Player() constructor function takes three arguments "playerElement", "playlistModel" and "configModel"
// Based on a PlaylistModel and a ConfigModel object it plays a presentation and displays it in the playerElement.
//
// Public methods:
// play()
// pause()
// stop()
// goTo( <subjectId> )
// getVariable( variableId )
// isPlaying()
// isPaused()
// isStopped()
// getConfigProperty()
// addEventHandler()
// clearEventHandler
// clearEventHandlers()
//
// ########################################################################################################################################################

// TODO:
// - when the player has nothing more to play it just simply stops leaving the player in state "playing".
//   would be better to show an end screen orso. and the buttons should be updated (for example the playbutton should
//   change to a replay button.


ximpel.Player = function( playerElement, playlistModel, configModel ){

	// The player element is the html elment to which all DOM elements will be attached (ie. the media types/overlays/etc.)
	this.$playerElement = ximpel.wrapInJquery( playerElement );

	
	// The "playlistModel" contains the data that the Player requires to play the presentation.
	// This is a PlaylistModel() object constructed by the parser based on the playlist file.
	this.playlistModel = playlistModel;

	// The "configModel" contains all the data related to configuration settings for the player.
	// This is a ConfigModel() object constructed by the parser based on the config file.
	this.configModel = configModel;

	// Stores the subject models. A subject model can be retrieved like so: this.subjectModels[subjectId].
	this.subjectModels = playlistModel.subjectModels;


	/*
	 *  Stores the ID of the subject that is to be started when the player starts.
	 *  This is needed in order to allow a user to revisit XIMPEL and continue from the last subject they visited
	 *  during the previous session.
	 */
	this.firstSubjectModel = this.getFirstSubjectModel();

	// The available media types is an object containing all the mediaTypeRegistration objects. These registrations contain data about the implemented 
	// media types. A mediaTypeRegistration object can be retrieved like: this.availableMediaTypes[<mediaTypeName>]. For instance: availableMediaTypes['video']
	// For the Player the most important data in the registration object is a pointer to the constructor of the media type. A new instance of a media type
	// can be created like this: var videoInstance = new availableMediaTypes['video'].mediaTypeConstructor();
	this.availableMediaTypes = ximpel.availableMediaTypes; 

	// The mediaItems object will contain all the media type instances (ie. it will contain all the Video() objects, Audio() objects, etc.)
	// A media item is refered to by its mediaId, where the mediaId is a property in the mediaModel that is filled by the parser (ie. each 
	// <video>, <audio>, etc. gets a unique ID). Referring to a media item is done like this: this.mediaItems[<mediaId>]  The result is a media
	// instance (for example a Video() object or an Audio() object) on which methods like play(), pause() and stop() can be called.
	// The media instances will be created and added to the mediaItems object by the constructMediaItems() function.
	this.mediaItems = {};

	// Will hold the subjectModel that is currently being played.
	this.currentSubjectModel = null;

	// The pubSub object is used internally by the player for registering event handlers and publishing events to the registered handlers.
	this.pubSub = new ximpel.PubSub();

	// The ximpel player can keep track of variables. Variables can be declared and modified using the <score> or <variable> tag in the playlist.
	this.variables = [];

	// The state of the player (ie. paused/playing/stopped)
	this.state = this.STATE_STOPPED;

	// Each subject contains exactly one main sequenceModel that is to be played. The sequencePlayer plays such a sequeceModel.
	// Note that a sequenceModel itself may contain: media items, other sequence models and parralel models. However, since
	// the ximpel Player always has one main sequence, it can just tell the sequence player to play/pause/stop that main
	// sequence without worrying about how complex that sequence may be.
	this.sequencePlayer = new ximpel.SequencePlayer( this );

	// Add an event handler function for when the sequence player finished playing a sequence. When a sequence has ended
	// because all the items in the sequence have finished playing, the sequence player will trigger this event.
	this.sequencePlayer.addEventHandler( this.sequencePlayer.EVENT_SEQUENCE_END, this.handleSequencePlayerEnd.bind(this) );

	// Do some stuff to initialize the player to make it ready for use.
	this.init();
};
ximpel.Player.prototype.STATE_PLAYING = 'state_player_playing';
ximpel.Player.prototype.STATE_PAUSED = 'state_player_paused';
ximpel.Player.prototype.STATE_STOPPED = 'state_player_stopped';
ximpel.Player.prototype.EVENT_PLAYER_END = 'ended';



// init() initializes the player once when the Player is constructed, but is never called again after that.
ximpel.Player.prototype.init = function(){

	// Create an instance (mediaItem) for each mediaModel (ie. for each media tag present in the the playlist.
	// This fills the this.mediaItems object with media items. A mediaItem can then be returned by doing:
	// this.mediaItem[ mediaModel.mediaId ]
	this.constructMediaItems();

	// This applies all variable modifiers on the playlist model which will initialize the
	// variables with a value. They are stored in: this.variables
	this.applyVariableModifiers( this.playlistModel.variableModifiers );



	return this;
};



// Reset the player to bring it back into a state where it was in when the Player() was just constructed
// and initialized. After this method the player is in a stopped state and the play() method can be called 
// as if it was the first time the player was being played.
ximpel.Player.prototype.reset = function( clearRegisteredEventHandlers ){

	this.state = this.STATE_STOPPED;
	this.currentSubjectModel = null;
	
	// Stop the sequence player. This resets the sequence player to its initial state.
	this.sequencePlayer.stop();

	// Re-initialize variables.
	this.variables = [];
	this.applyVariableModifiers( this.playlistModel.variableModifiers );

	// If specified then the event handlers registered on the Player()'s pubSub will be reset.
	if( clearRegisteredEventHandlers ){

		this.clearEventHandlers();
	}
};



// Start playback of the player. If the player was paused it will resume instead.
ximpel.Player.prototype.play = function(){
	if( this.isPlaying() ){

		ximpel.warn("Player.play(): play() called while already playing.");
		return this;
	} else if( this.isPaused() ){

		this.resume();

		return this;
	}

	// indicate the player is in a playing state.
	this.state = this.STATE_PLAYING;

	// This play() method is called from a stopped state so start playing the first subject.
	this.playSubject( this.firstSubjectModel );
	return this;
};



// Start playing a given subjectModel. 
ximpel.Player.prototype.playSubject = function( subjectModel ){

	// Set the specified subjectModel as the current subject model
	this.currentSubjectModel = subjectModel;
	ximpel.insertVisitedToDatabase(this.currentSubjectModel.subjectId, this.currentSubjectModel.description);
	// Each subject contains exactly one sequence model. The sequencePlayer plays such a sequence model. The sequence model itself may contain
	// one or more media models and parrallel models which in turn may contain sequence models again. This playback complexity is all handled by
	// the sequence player so we do not need to worry about that here, we just need to tell the sequence player to start playing the sequence
	// of our subject.
	var sequenceModel = subjectModel.sequenceModel;

	// In the playlist you can define variables/scores to be changed when a subject starts. When you do this
	// the parser will add a variableModifier object and store it in a list of variableModifiers for that subject.
	// When the subject is requested to be played we need to apply these variable modifiers to the variables, This
	// is what we do next.
	this.applyVariableModifiers( subjectModel.variableModifiers );

	// Then finally tell the sequence player to start playing the sequence model of our subject.
	this.sequencePlayer.play( sequenceModel );
};



// Resume playback of the player.
ximpel.Player.prototype.resume = function(){

	// Ignore this resume() call if the player is already in a playing state.
	if( !this.isPaused() ){

		ximpel.warn("Player.resume(): resume() called while not in a paused state.");
		return this;
	}
	// Indicate the player is now in a playing state again.
	this.state = this.STATE_PLAYING;

	// Resume the sequence player.
	this.sequencePlayer.resume();

	return this;
};



// Pause playback of the player.
ximpel.Player.prototype.pause = function(){

	// Ignore this pause() call if the player is not in a playing state.
	if( ! this.isPlaying() ){
		ximpel.warn("Player.pause(): pause() called while not in a playing state.");
		return this;
	}

	// Indicate the player is now in a paused state.
	this.state = this.STATE_PAUSED;

	// Pause the sequence player.
	this.sequencePlayer.pause();

	return this;
};



// Stop playback of the player.
ximpel.Player.prototype.stop = function(){
	// Ignore this stop() call if the player is already in the stopped state.
	if( this.isStopped() ){

		ximpel.warn("Player.stop(): stop() called while already in a stopped state.");
		return this;
	}

	// Indicate the player is now in a stopped state.
	this.state = this.STATE_STOPPED;

	// Resets the player to the point it was in right after its construction and after the init() method.
	// After the reset its ready to be play()'ed again.
	this.reset();

	return this;
};



// Jump to the subject with the given subjectId. This method can be called at anytime from anywhere and
// will cause the player to stop playing what it is playing and jump to the specified subject.
ximpel.Player.prototype.goTo = function( subjectId ){

	var subjectModel = this.subjectModels[subjectId];
	if( !subjectModel ){
		ximpel.warn("Player.goTo(): Cannot play a subject with subjectId '" + subjectId + "'. There is no subject with that id.");
		return;
	}

	this.playSubject( subjectModel );
	return this;
};



// Retrieve a variable with a given id or the default variable if no id is given.
ximpel.Player.prototype.getVariable = function( variableId ){
	return this.variables[variableId];
};



// This method takes an array of variable modifiers and applies each of them. After this method each of the modifiers have been applied.
// See function: applyVariableModifier() for more info on what a variable modifier is.
ximpel.Player.prototype.applyVariableModifiers = function( variableModifiers ){

	$.each( variableModifiers, function( index, value ){

		var variableModifier = variableModifiers[index];
  		this.applyVariableModifier( variableModifier );
	}.bind(this) );
};



// This function applies one variableModifier. A variable modifier contains:
// - A variable id which indicates the variable to modify
// - An operation that changes the value of the variable
// - The value used by the operation
// For example when: id="score1", operation="add", value="6", the variable modifier adds 6 to the "score1" variable.
ximpel.Player.prototype.applyVariableModifier = function( variableModifier ){

	var currentVariableValue = this.variables[ variableModifier.id ];

	// If the variable to which the modification is applied hasn't been defined yet, then we define it right here to 0.
	if( currentVariableValue === undefined ){

		this.variables[ variableModifier.id ] = 0;
		currentVariableValue = 0;
	}

	// Apply the operation.
	switch( variableModifier.operation ){

		case variableModifier.OPERATION_SET:
			var newValue = variableModifier.value;
			break;
		case variableModifier.OPERATION_ADD:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue += Number( variableModifier.value );
			break;
		case  variableModifier.OPERATION_SUBSTRACT:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue -= Number( variableModifier.value );
			break;
		case variableModifier.OPERATION_MULTIPLY:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue *= Number( variableModifier.value );
			break;
		case variableModifier.OPERATION_DIVIDE:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue /= Number( variableModifier.value );
			break;
		case variableModifier.OPERATION_POWER:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue = Number( Math.pow(newValue, variableModifier.value ) );
			break;
		default:
			var newValue = currentVariableValue;
	}

	// Store the new value of the variable
	this.variables[ variableModifier.id ] = newValue;
};




// Return whether the player is playing.
ximpel.Player.prototype.isPlaying = function(){

	return this.state === this.STATE_PLAYING;
};



// Return whether the player is paused.
ximpel.Player.prototype.isPaused = function(){

	return this.state === this.STATE_PAUSED;
};



// Return whether the player is stopped.
ximpel.Player.prototype.isStopped = function(){

	return this.state === this.STATE_STOPPED;
};



// When in the playlist you specify a leadsTo attribute and/or <leadsTo> elements in a subject, overlay or media item then the parser
// will construct a list of leadsToModels and stores it in that subjectModel, overlayModel or mediaModel. Whenever that subject
// finishes, the overlay is clicked or the media item finishes, XIMPEL will look at that list of leadsToModels and based on that
// list it determines which leadsTo value to use next (ie. which subject to play next).
// A LeadsToModel consists of:
// - a subject attribute specifying the subject to play.
// - a condition attribute specifying the condition under which this leadsTo attribute should be used. When
//   no condition is specified its the default leadsTo value that will be used if no other leadsTo model's condition is met.
// The determineLeadsTo() method is the method that determines which of the leadsTo models should be used.
// Note that both the leadsTo attribute and <leadsTo> elements are converted to leadsTo models by the parser so they
// are basically the same, except when using the leadsTo attribute you cannot specify a condition. So this is
// usually the default leadsTo value.
// Return value: the subjectId that should be played next or null if there is no subject to play next.
ximpel.Player.prototype.determineLeadsTo = function( leadsToModels ){

	var defaultLeadsTo = null;
	// Loop over all leadsToModels in the given array and find out which leadsTo value should be used (if any)

	for( var i=0; i<leadsToModels.length; i++ ){
		
		var leadsToModel = leadsToModels[i];


		// If the current leadsToModel has no condition specified, then its the default leadsToModel.
		// The default is only used when all of the conditional leadsToModels evaluate to false. In other
		// words: the condition leadsToModels have precedence over the default leadsToModel.
		// We store the default leadsTo subject-id and continue evaluating the other leadsToModels.
		if( !leadsToModel.conditionModel ){
			defaultLeadsTo = leadsToModel.subject;

			/*
			 *  If a subject is marked as a dynamic branch, it is necessary to determine which subjects should
			 *  become available to the viewer at this time.
			 */
			if ((this.subjectModels[defaultLeadsTo].dynamicBranch == 'true')){

				var itemsToPrepare = this.determineBranches(defaultLeadsTo);


				/*
				 *  Any dynamically added overlays need to be removed first. If a page is visited again
				 *  we need to remove the dynamically added overlays, otherwise they will be shown again
				 */
				this.clearOldDynamicOverlays(defaultLeadsTo);
				
				/*
				 *  The previous overlays for this subject have been removed.
				 */
				this.prepareOverlays(defaultLeadsTo, itemsToPrepare);

			}
			continue;
		}

		// The leadsToModel has a condition so we evaluate it and if the condition is true,
		// then we return this leadsToModel as the result.
		var conditionIsTrue = this.evaluateCondition( leadsToModel.conditionModel );
		if( conditionIsTrue ){

			return leadsToModel.subject;
		}
	}

	return defaultLeadsTo;
};




// This method evaluates a conditionModel. The condition model specifies the condition/expression that
// is to be evaluated. By using a conditionModel object as a wrapper around the actual condition/expression 
// we allow future changes in how the condition is represented. Right now the conditionModel just takes
// a string wich might contain templated variable names in the form: {{variableName}}
// The templated variable names should correspond with a variable declared in the playlist. If no such
// variable exists then it is not replaced. After the variable values have replaced the templated variable
// names, the eval method is used to execute the expression and the result (true or false) is returned.
ximpel.Player.prototype.evaluateCondition = function( conditionModel ){

	var condition = conditionModel.condition;
 	var parsedCondition = condition;

	// First we retrieve an array of all the templated variable names. Templated variables look like this {{variableName1}}
	// So for the string: "{{x}}+{{x}}=={{y}}" we get an array: ['x','x','y']
    var regex = /\{\{(\w+)\}\}/g;
	var variableNames = [];
	var variableNamesTemplated = [];
    while( match = regex.exec(condition) ){

		variableNames.push( match[1] );
		variableNamesTemplated.push( '{{' + match[1] + '}}' );
	}

	// Then we get an array containing the values corresponding the given variable names.
	var variableValues = [];
	for( var i=0; i<variableNames.length; i++ ){

		var variableName = variableNames[i];
		variableValues[i] = this.variables[variableName];
	}

	// This variable will indicate when a variable in the condition failed to be replaced because the
	// variable did not exist for instance.
	var failedToTemplateCondition = false;

	// Then we replace each of the templated variables with the variable values.
	// The result is a string where all the variable names have been replaced with
	// the corresponding variable values.
    $.each( variableNamesTemplated, function( index, key ){

		// If the variable value of the current variable is not null and not undefined then
    	// we insert the value into the string.
    	if( variableValues[index] !== undefined && variableValues[index] !== null ){
        	parsedCondition = parsedCondition.replace( key, variableValues[index] );
        } else{
        	// If the variable template could not be replaced because the template did not
        	// correspond to an existing XIMPEL variable, then we set the failedToTemplateCondition
        	// flag to true which indicates that the condition can not be evaluated properly.
        	failedToTemplateCondition = true;
        }
    });

    // the condition string contained variable templates that did not correspond to
    // existing XIMPEL variables. So this condition cannot be evaluated and we
    // return false to indicate the condition is not met.
    if( failedToTemplateCondition === true ){

		return false;
    }

    // We have a condition that properly parsed, so we eval() them.
	var result = eval( parsedCondition );

	// If the expression returned a non boolean value then we consider the condition to be false.
	return (result === true || result === false) ? result : false;
};



// Determine which subject id should be played next.
ximpel.Player.prototype.determineNextSubjectToPlay = function(){
	
	var leadsTo = this.determineLeadsTo( this.currentSubjectModel.leadsToList );
	// returns a subject id.
	return leadsTo;
};



// This method handles the end event of the sequence player.
ximpel.Player.prototype.handleSequencePlayerEnd = function(){
	// The sequence player has nothing more to play. If the current subject has a leadsTo
	// attribute, then we jump to that subject.
	var subjectId = this.determineNextSubjectToPlay();
	if( subjectId ){

		 this.goTo( subjectId );
	}

	// There is nothing more to play.... we may want to present an end screen here.

	// Publis the player end event. Any (third party) code that registered a handler for this event using
	// addEventHandler() will have its handler called.
	this.pubSub.publish( this.EVENT_PLAYER_END );
};



/*
 *  This method will return the subject that needs to be shown first, when XIMPEL starts.
 *  If it is a returning user, the last visited subject will be set and that will be where XIMPEL will start
 *  playing. However, if it is not a returning user, the method will return the default subject, which is the
 *  first subject from the playlist.
 */
ximpel.Player.prototype.getFirstSubjectModel = function(){
	// The first subject to be played is specified in the playlist model (determined in the parser).

	if (ximpel.lastSubject.subject == undefined){

		return this.playlistModel.subjectModels[ this.playlistModel.firstSubjectToPlay ];
	}
	else {

		var subjects = [];
		subjects.push(ximpel.lastSubject);

		/*
		 *  It is a returning user. Although the last visited subject is now known, it is still necessary
		 *  to determine which subjects are available to the user, following the first subject.
		 *  The overlays for all available branches need to be determined and the overlays need to be generated.
		 */
		this.determineLeadsTo(subjects);
		return this.playlistModel.subjectModels[ ximpel.lastSubject.subject ];
	}

};

// Add an event handler to listen for events that this Player object throws.
ximpel.Player.prototype.addEventHandler = function( eventName, func ){

	switch( eventName ){
		case this.EVENT_PLAYER_END:
			return this.pubSub.subscribe( this.EVENT_PLAYER_END, func ); break;
		default:
			ximpel.warn("Player.addEventHandler(): cannot add an event handler for event '" + eventName + "'. This event is not used by the player.");
			break;
			return;
	}
};

// Clear all event handlers that have been registered to this player object.
ximpel.Player.prototype.clearEventHandlers = function( callback ){

	this.pubSub.reset();
	return this;
};

// Cancels a registered event handler for the given eventName and handler function.
ximpel.Player.prototype.clearEventHandler = function( eventName, callback ){

	this.pubSub.unsubscribe( eventName, callback );
};


// The constructMediaItems function takes the list of mediaModels from the playlist object and creates an instance of a media type for each
// mediaModel. These instances are added to the mediaItems property of the player. To access an instance of a media type
// you can do: var mediaItemInstance = this.mediaItems[mediaId]; The mediaId is stored within a mediaModel (determined by the parser).
ximpel.Player.prototype.constructMediaItems = function(){

	var mediaModels = this.getMediaModels();
	// For each media model create a media item and store as this.mediaItems[ mediaModel.mediaId ]
	mediaModels.forEach( function( mediaModel ){

		var mediaTypeRegistration = this.availableMediaTypes[ mediaModel.mediaType ];
		var mediaItem = new mediaTypeRegistration['mediaTypeConstructor']( mediaModel.customElements, mediaModel.customAttributes, this.$playerElement, this );
		this.mediaItems[ mediaModel.mediaId ] = mediaItem;
	}.bind(this) );
	
	return this;
};



// returns the player element of this Player() object.
ximpel.Player.prototype.getPlayerElement = function(){

	return this.$playerElement;
};



// Returns the array of mediaModels for the current playlist.
ximpel.Player.prototype.getMediaModels = function(){

	return this.playlistModel.mediaList;
};



// Returns a config property that was specified in the config file or in the playlist file within the <config> element.
// For example getConfigProperty( showControls ) returns the value specified in the config file. For instance: <showControls>true</showControls>
// Return value: the value of the config property or null if the property name doesn't exist.
ximpel.Player.prototype.getConfigProperty = function( propertyName ){

	var value = this.configModel[propertyName];
	if( value !== undefined ){

		return value;
	} else{

		return null;
	}
};

/*
 *  This method takes the id of a subject and determines which subjects are available directly following the subject.
 *
 */
ximpel.Player.prototype.determineBranches = function (defaultLeadsTo){

	// store the subjects we want to make available here
	var arrayOfBranches = [];
	for (var i in this.subjectModels) {

		// don't consider a subject if it is the current subject
		if (this.subjectModels[i].subjectId == defaultLeadsTo){

			continue;
		}

		// don't consider a subject if it has been visited before
		if (this.hasVisitedBefore(this.subjectModels[i].subjectId)){

			continue;
		}

		// if the subject is a quiz, we will ignore it for now. Quizzes will be processed later on
		else if (this.subjectModels[i].quiz == 'true'){

			continue;
		}

		/*
		 * ignore subjects that don't have input tags or outputs tags. These subjects should only be shown if they
		 * are directly linked to
		 */
		else if ((this.subjectModels[i].inputTagsList.length == 0) && (this.subjectModels[i].outputTagsList.length==0)){

			continue;
		}

		/*
		 * Don't consider subjects that do not have a parentOverlayModel, such as branches used by
		 * quizzes for showing correct and incorrect answers
		 */
		else if (this.subjectModels[i].parentOverlayModel == null){

			continue;
		}

		else if ((this.currentSubjectModel) && (this.currentSubjectModel.subjectId == this.subjectModels[i].subjectId)) {


			continue;
		}

		/*
		 *  The subject being considered is a candidate.
		 */
		else {

			/*
			 *  The subject's tags need to be checked, to see if it really should be shown
			 */
			if (this.determineBranchMatch(this.subjectModels[i].subjectId)) {

				arrayOfBranches.push(this.subjectModels[i].subjectId);
			}
			else {

				continue;
			}
		}
	}

	// call a function to insert quizzes into the arrayOfBranches
	this.getQuizzes(this.subjectModels, arrayOfBranches);
	return arrayOfBranches;
};

/*
 *  This method takes a subjectID and returns true if the subject should become available to the viewer.
 */
ximpel.Player.prototype.determineBranchMatch = function (subjectId){

	// get the input tags for the subject being considered
	var inputTagsInSubject = this.subjectModels[subjectId].inputTagsList;

	// get the output tags for the subject being considered
	var outputTagsInSubject = this.subjectModels[subjectId].outputTagsList;

	/*
	 *   return true if the subject does not require any input tags and the user also has no tags
	 *   This would typically be one of the first subjects in the playlist, when the user has not received any tags yet
	 */
	if ((inputTagsInSubject.length == 0) & (Object.keys(ximpel.userTags).length == 0)){

		return true;
	}

	/*
	 * return false if the subject requires input tags and the user does not have any tags.
	 * This subject should not be available yet, because the subject requires the user to have a tag whereas the
	 * use does not have any tags yet.
	 */
	else if (!(inputTagsInSubject.length == 0) & (Object.keys(ximpel.userTags).length == 0)){

		// the subject requires tags, but the user has none

		return false;
	}

	/*
	 *  The subject requires tags and the user has tags. However, it is still necessary to determine if the user's tags
	 *  meet the subjec's tag requirements.
	 */
	else {

		/*
		 *  Initialy the user has all the mandatory tags, untill we find that the user is missing a required tag and
		 *  tagvalue.
		 */
		var hasAllMandatoryTags = true;

		/*
		 *  Initially the user does not have an optional tag, untill we find that the user has a tag that is
		 *  required by a subject as an optional tag. A user needs at least one optional tag and tag value.
		 */
		var hasOptionalTag = false;

		// iterate through all the input tags in the subject
		for (var i in inputTagsInSubject) {

			/*
			 *  if the subject has an optional tag, determine if the user has this optional tag and the correct
			 *  value for the tag. If the user has the optional tag and value, then the subject is a candidate
			 *  as far as the optional tags are concerned
			 */

			if (inputTagsInSubject[i].optional == 'true') {

				if (this.determineTagMatch(inputTagsInSubject[i]) == true){

					hasOptionalTag = true;
				}
			}

			/*
			 *  If the tag is mandatory, and the user has this tag and value, then we don't need to do anything yet,
			 *  but if the user does not have this mandatory tag, then the subject will not be a candidate, as
			 *  the user needs to have ALL the mandatory tags and values.
			 */
			else if (inputTagsInSubject[i].optional == 'false'){

				if (this.determineTagMatch(inputTagsInSubject[i]) == true){

				}

				else {

					hasAllMandatoryTags = false;
				}
			}
		}

		/*
		 *  If the subject has input tags, but none of them are optional, then we can say that the user has all the
		 *  required optional tags.
		 */
		if (!this.hasOptionalTags(inputTagsInSubject) & !(inputTagsInSubject.length == 0)) {


			hasOptionalTag = true;
		}

		/*
		 *  If the subject has input tags, but none of them are mandatory, then we can say that the user has all the
		 *  mandatory tags.
		 */
		if (!this.hasMandatoryTags(inputTagsInSubject) & !(inputTagsInSubject.length == 0)) {

			hasAllMandatoryTags = true;
		}

		/*
		 *  If the user has all the mandatory tags and at least one optional tag, then the subject should be made
		 *  available
		 */
		if ((hasAllMandatoryTags == true) & (hasOptionalTag == true) ) {

			return true;
		}

		/*
		 *  The user's tags do not meet the required input tags for this subject being considered, so we return false.
		 */
		else {

			return false;
		}
	}
};

/*
 *  This method takes a subjectId (leadsToModel) and an array of subjectID's (itemsToPrepare).
 *  The leadsToModel is a subject that will contain overlays linking the subjects in the itemsToPrepare array.
 *  The itemsToPrepare contains the subjects that the user should be able to visit next. For each item in the
 *  itemsToPrepare array, we need to take the parentOverlayModel and assign it to the leadsToModel's list of
 *  overlays. Once these overlays have been added to the subject, we need to re-order the height variable, in order
 *  to prevent them from being shown on top of each other.
 *
 */
ximpel.Player.prototype.prepareOverlays= function (leadsToModel, itemsToPrepare){

	// this is the subject that will contain the resulting overlays
	var itemWithBranch = this.subjectModels[leadsToModel];

	// iterate through the list of items to prepare
	for (item = 0; item < itemsToPrepare.length; item++) {

		// the current item from the list of items to prepare
		var nextItem = this.subjectModels[itemsToPrepare[item]];

		// the parent overlay model from the current item
		var branchModelOverlay = nextItem.parentOverlayModel;

		if (branchModelOverlay != null) {

			// create and empty leadsToList array
			branchModelOverlay['leadsToList'] = [];

			// create and empty, but necesssary variable modifiers array
			branchModelOverlay['variableModifiers'] = [];

			// add the branche's subject id to the subject's lead to list
			var leadTo = this.subjectModels[itemsToPrepare[item]].subjectId;
			branchModelOverlay['leadsToList'].push({"subject": leadTo});

			/*
			 *  if the subject, to which the overlays need to be added, contains a number if media objects,
			 *  we want to add the overlays to the last media object. This makes it possible to
			 *  insert media objects such as intro's and so forth, before presenting the overlay's to the
			 *  next subjects
			 */
			var numberInList = itemWithBranch.sequenceModel.list.length;
			itemWithBranch.sequenceModel.list[numberInList - 1].overlays.push(branchModelOverlay);
		}
	}

	/*
	 *  Now that all the overlays have been added to the subject, we want to avoid the overlays from being shown
	 *  on top of each other. We can do this by increasing the height value of each overlay, so the overlays are
	 *  shown below one another.
	 */
	var startY = parseInt(itemWithBranch.dynamicOverlay.startY);
	var spacing = parseInt(itemWithBranch.dynamicOverlay.spacing);
	for (var i in this.subjectModels[leadsToModel].sequenceModel.list){

		var overlays = this.subjectModels[leadsToModel].sequenceModel.list[i].overlays;

		for (var j in overlays){

			if (!(overlays[j].length==0)){

				overlays[j].y = startY;

				startY = (startY + (spacing + overlays[j].height));
			}
		}
	}
};

/*
 *  This method takes a list of tags and returns true if the list contains any optional tags
 */
ximpel.Player.prototype.hasOptionalTags = function(tagList){

	var hasOptionalTags = false;
	for (var i in tagList){

		if (tagList[i].optional == "true") {

			hasOptionalTags = true;
		}
	}
	return hasOptionalTags;
};

/*
 *  This method takes a list of tags and returns true of the list contains any mandatory tags.
 */
ximpel.Player.prototype.hasMandatoryTags = function(tagList){

	var hasMandatoryTags = false;
	for (var i in tagList){

		if (tagList[i].optional == 'false') {

			hasMandatoryTags = true;
		}
	}
	return hasMandatoryTags;
};

/*
 *  This method takes a tag and determines if the user has the tag and the value. If the user has the tag and the value,
 *  it returns true.
 */
ximpel.Player.prototype.determineTagMatch = function (inputTag){

	var found = false;

	for (var i in ximpel.userTags){
		if ((ximpel.userTags[i].tagName == inputTag.tagName) && (ximpel.userTags[i].tagValue == inputTag.tagValue)) {

			found = true;
		}
	}
	return found;
};

/*
 *  This method takes a tag and determines if the user has the tag and if the value of this tag is smaller than
 *  the tag value in the given tag. This is used to determine when a quiz should be shown or not. A user can perform
 *  a number of quizzes until a certain tag value has been reached. For instance, if a user fails a quiz, another
 *  quiz can be performed.
 */
ximpel.Player.prototype.determineQuizTagMatch = function (inputTag){

	var found = false;

	for (var i in ximpel.userTags){
		if ((ximpel.userTags[i].tagName == inputTag.tagName) && (ximpel.userTags[i].tagValue < inputTag.tagValue)) {

			found = true;
		}
		else {

		}
	}
	return found;
};

/*
 *  This method takes the array of subjectModels and the array containing the array of subjects (branches) that the
 *  user can visit next. This method will append a quiz subject to the array of branches, if it is determined that the
 *  user should be able to perform a quiz at this time. A quiz can be done, if the user has the required tags and if
 *  the quiz has not already been done before.
 */
ximpel.Player.prototype.getQuizzes = function(subjectModels, arrayOfBranches) {

	// iterate through the array of quizzes
	for (var i in ximpel.quizzes){

		// only consider a quiz if it has not yet been performed (visited) already
		if (ximpel.quizzes[i].visited == false){

			var userHasAllMandatoryTags = true; // true untill we find one that makes it false
			var userHasOptionalTags = false; // false untill we find one that makes it true

			var inputTags = this.subjectModels[ximpel.quizzes[i].subjectId].inputTagsList;

			// iterate through the array of input tags for the quiz subject
			for (var j in inputTags){

				/*
				 *  If the input tag is mandatory, we need to determine if the user has this mandatory tag and value
				 */
				if (inputTags[j].optional=='false'){


					if (!this.determineQuizTagMatch(inputTags[j])){

						// the user does not have this mandatory tag
						userHasAllMandatoryTags = false;
					}
				}
				/*
				 *  If the input tag is optional, then the user has al least one of the required
				 *  optional tags and value.
				 */
				else {

					if (this.determineQuizTagMatch(inputTags[j])){
						userHasOptionalTags = true;
					}
				}
			}

			// if the subject does not have any optional tags, then we consider the user to have all the optional tags.
			if (!this.hasOptionalTags(inputTags)) {

				userHasOptionalTags = true;

			}
			// if the subject does not have any mandatory tags, then we consider the user to have all the mandatory tags.
			if (!this.hasMandatoryTags(inputTags)) {

				userHasAllMandatoryTags = true;
			}
			/*
			 *  If the user has all the mandatory and optional tags, then we push this quiz to the array of branches
			 *  and exit the loop. We thus, show the first elegible quiz.
			 */
			if (userHasAllMandatoryTags & userHasOptionalTags){

				arrayOfBranches.push(ximpel.quizzes[i].subjectId);

				break;
			}
		}
	}
};


/*
 *  This method is called when a user chooses an answer during a quiz. The answer overlay can contain a number of
 *  output tags that will influence the user's tags.
 */
ximpel.Player.prototype.addQuizScoreToUserTags = function (variableModifiers) {

	for (var i in variableModifiers){

		this.applyQuizScore(variableModifiers[i]);
	}
};

/*
 *  This method takes an output tag and processes the value and operation and applies it to the user's tags.
 *  Next, it will insert the tag into the database for the current user.
 */
ximpel.Player.prototype.applyQuizScore = function(variableModifier) {


	// if the user does not have the resulting tag, it needs to be created first, before it can be applied
	if (!ximpel.userTags[variableModifier.id]){

		ximpel.userTags[variableModifier.id] = {};
		ximpel.userTags[variableModifier.id].tagName = variableModifier.id;
		ximpel.userTags[variableModifier.id].tagValue = 0;
	}
	var currentVariableValue = ximpel.userTags[variableModifier.id].tagValue;

	// Apply the operation.
	switch( variableModifier.operation ){

		case variableModifier.OPERATION_SET:
			var newValue = variableModifier.value;
			break;
		case variableModifier.OPERATION_ADD:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue += Number( variableModifier.value );
			break;
		case  variableModifier.OPERATION_SUBSTRACT:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue -= Number( variableModifier.value );
			break;
		case variableModifier.OPERATION_MULTIPLY:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue *= Number( variableModifier.value );
			break;
		case variableModifier.OPERATION_DIVIDE:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue /= Number( variableModifier.value );
			break;
		case variableModifier.OPERATION_POWER:
			var newValue = Number(currentVariableValue) === NaN ? 0 : Number(currentVariableValue);
			newValue = Number( Math.pow(newValue, variableModifier.value ) );
			break;
		default:
			var newValue = currentVariableValue;
	}

	// Store the new value of the variable
	ximpel.userTags[variableModifier.id].tagValue = newValue;

	// create an array, containing the resulting tag and call the method that can insert the tag into the database.
	var userTagsForDatabase = [];
	userTagsForDatabase.push({"tagName" : variableModifier.id,"tagValue" : newValue});
	ximpel.insertUserTagsToDatabase(userTagsForDatabase);
};

/*
 *  This method takes a subject ID and determines if the user has visited the subject before. It the subject
 *  has been visited before, true is returned.
 */
ximpel.Player.prototype.hasVisitedBefore = function(subjectID){

	var visited = false;


	for (var i in ximpel.visited){

		if (ximpel.visited[i] == subjectID){

			visited = true;
		}
	}

	return visited;
};


/*
 *  If a subject is re-visisted, we need to clear the dynamically added overlays that were added previously.
 *  However, we do not want to remove overlays that were added to the subject in the playlist.
 */
ximpel.Player.prototype.clearOldDynamicOverlays = function(defaultLeadsTo){

	for (var i in this.subjectModels[defaultLeadsTo].sequenceModel.list){

		var overlays = this.subjectModels[defaultLeadsTo].sequenceModel.list[i].overlays;

		for (var j in overlays){

			for (var k in overlays[j].leadsToList) {

				// check if the overlay was added dynamically

				if ((overlays[j] instanceof ximpel.ParentOverlayModel)){

					// clear the overlay
					overlays[j] = new Array(0);
				}
			}
		}
	}
};
