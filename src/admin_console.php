<?php
/*
 *  This is the user manager for XIMPEL. It is only used by administrators of the XIMPEL application
 *  It can be used for the following:
 *  * Change the name and password of the XIMPEL administrator.
 *  * Delete Users (and all their history)
 *  * Delete users' history
 *  * Change the information of a user (username, full name, password and email address)
 *  * View the users' history.
 * When viewing a user's history, the following can be viewed:
 * * The subject a user has visited. The <description> attribute from the playlist is also used, to describe the
 *   subject
 * * The tags and their values, as they are assigned to a user
 * * The links that a user visits
 * These items are sorted by the timestamp when they were added to the database
 */

// Include the checkSessionID function in order to determine if the administrator is logged in
include('./php/validate_admin.php');

/*
 *  This function open a connection to the databsase and returns a handle to the database.
 */
function getDB(){

    $file_db = new PDO('sqlite:./db/ximpel.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $file_db;
}

/*
 *  If the administrator is not logged on, redirect to the logon page for administrator
 */
if (!checkSessionID()){

    header('Location: admin_login.php');
}


?>
<!--

This is the HTML contents of the administrator console
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XIMPEL User Manager</title>
    <link rel="stylesheet" href="./css/admin_console.css" type="text/css" />
    <script>
        // advise the administrator to logout from the console, rather than navigate away from it.
        window.onbeforeunload = function() {

            return "It is better to logout properly!";
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="./js/admin_console.js" ></script>



</head>
<body>
<h1>This is the XIMPEL user manager</h1>
<input id="btnLogout" type="button" value="Logout"><br><br>
<fieldset id="fieldsetAdministratorUser">
    <legend id="legendAdministratorUser"><b>Administrator account</b></legend>
        <label for="txtAdminName"><b>Name</b></label><br>
        <input type="text" id="txtAdminName" name="txtAdminName"><br>
        <label for="txtAdminPassword"><b>Password</b></label><br>
        <input type="text" id="txtAdminpassword" name="txtAdminpassword"><br>
        <input id="btnSaveChanges" type="button" value="Save changes" disabled><br>
</fieldset><br>
<fieldset id="fieldsetUsersTable" hidden>
    <legend id="legendUsers">
        <b>XIMPEL users</b>
    </legend>
    <span id="no_users" hidden>There are currently no users</span>
    <div id="tabelUsers">

    </div>
</fieldset>
<br>
<fieldset id="fieldsetUserHistory" hidden>
    <legend id="legendHistory">
        <b>User History</b>
    </legend>
    <div id="divCheckBoxes" hidden>
    <input id="cbSubjects" type="checkbox" name="subject" value="subject" checked="checked"/>Subjects
    <input id="cbLinks" type="checkbox" name="link" value="link" checked="checked"/>Links
    <input id="cbTags" type="checkbox" name="tags" value="tag" checked="checked"/>Tags
    </div>
    <span id="no_history" hidden>There is no history for this user.</span>
    <div id="tabelUserHistory">

    </div>
</fieldset>



</body>
</html>
