<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Account</title>
    <link rel="stylesheet" href="./css/new_user.css" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="./js/new_account.js"></script>
</head>
<body>
<fieldset>
    <legend>Enter new account details</legend>
    <form id="frmNewAccount" action="ximpel.php" method="post">
        <label for="user_name">Username</label><br>
        <input id="user_name" name="user_name" type="text" pattern="[A-Za-z0-9]{4,25}" required placeholder="username (no spaces)" title="Enter a short username (at least four characters), without any spaces!">
        <span hidden id="txtNameTaken" >Username already taken</span>
        <br><br>

        <label for="txtFullName">Full name</label><br>
        <input id="txtFullName" name ="txtFullname" type="text" required placeholder="full name" title="Enter your full name"><br><br>

        <label for="txtEmail">Email address</label><br>
        <input id="txtEmail" name ="txtEmail" type="email" required placeholder="email address" title="Enter your email address"><br><br>

        <label for="password">Password</label><br>
        <input id="password" name="password" type="password" pattern="[A-Za-z0-9]{4,25}" required placeholder="password, at least four letters and or numbers" title="Enter a password of at least 4 characters (alphanumeric)"><br><br>

        <label for="txtPwd2">Repeat password</label><br>
        <input id="txtPwd2" name="txtPwd2" type="password" pattern="[A-Za-z0-9]{4,25}" required placeholder="repeat the same password" title="Repeat the password">
        <span hidden id="txtPwdMismatch" >Password does not match!</span><br><br>

        <input id="btnSubmitNewUser" name="btnSubmitNewUser" type="submit" value="Create account">
    </form>
</fieldset>
</body>
</html>