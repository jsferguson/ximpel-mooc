<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 06/06/16
 * Time: 09:37
 */
function getDB(){

    $file_db = new PDO('sqlite:./db/ximpel.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $file_db;
}

function checkAdmin($_in_name, $_in_password){

    $output = validateAdmin($_in_name, $_in_password);
    $result = array();
    $result['success'] = false;
    $result['error'] ="A problem occurred";

    if ($output['success'] == true){

        $result = getSessionID($_in_name, $_in_password);
    }

    return $result;
}

function validateAdmin($_in_name, $_in_password){

    $output = array();
    try {

        $file_db = getDB();
        $query = "select id from AdminUsers where name = :name and password = :password;";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':name', $_in_name);
        $stmt->bindParam(':password', $_in_password);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;
        if ($data ){

            $output['success'] = true;

            return $output;
        }
        else {

            $output['success'] = false;
            $output['error'] = "Unknown combination!";
            return $output;
        }

    }
    catch (Exception $e){

        $output['success'] = false;
        $output['error'] = $e->getMessage();
        return $output;

    }
}


function getSessionID($_in_name, $_in_password){

    $output = array();


    session_start();
    session_regenerate_id();
    $sessionID = session_id();
    $date = date_create();

    try {

        $file_db = getDB();
        $query = "insert into AdminSessions (admin_id, session_id, timestamp) values ((select id from AdminUsers where name = :name and password = :password), :session_id, :timestamp);";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':name', $_in_name);
        $stmt->bindParam(':password', $_in_password);
        $stmt->bindParam(':session_id', $sessionID);
        $stmt->bindParam(':timestamp', date_timestamp_get($date));
        $stmt->execute();

        $output['success'] = true;
        return $output;

    }
    catch (Exception $e){

        $output['success'] = false;
        $output['error'] = $e->getMessage();
        return $output;

    }
}


if (isset($_POST['user_name']) && (isset($_POST['password']))){

    echo json_encode(checkAdmin($_POST['user_name'], $_POST['password']));


}
else {

    echo <<<EOT
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Administrator Login</title>
    <link rel="stylesheet" href="./css/admin_login.css" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="./js/logon_admin.js"></script>
</head>
<body>

<fieldset>
    <legend>Logon with your XIMPEL administrator account</legend>
    <form id="frmLogon" method="post" action="admin_console.php">
        <span id="txtAccountUnknown" hidden>Invalid combination!</span><br>
        <label for="user_name">Your username</label><br>
        <input id="user_name" name="user_name" required autofocus><br><br>
        <label for="password">Your password</label><br>
        <input id="password" type="password" name="password" required><br>
        <input id="btnSubmit" type="submit" value="Logon">
    </form>
</fieldset>
</body>
</html>
EOT;

}

?>


