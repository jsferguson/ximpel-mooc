/*
 *  This is the script for the user logon screen.
 *  It will first validate the user's credentials. If these are incorrect, the user will see a message and will
 *  remain on this screen. If the credentials are valid, the user will be directed to the ximpel.php screen and
 *  XIMPEL will start running.
 */
$(function(){

    var user_name           = $('#user_name');
    var password            = $('#password');
    var frmLogon            = $('#frmLogon');
    var txtAccountUnknown   = $('#txtAccountUnknown');
    var btnSubmit           = $('#btnSubmit');
    frmLogon.submit(function(event) {

        if(!frmLogon.valid){

           event.preventDefault();
        }




        var formData = {

            "user_name"         : user_name.val(),
            "password"          : password.val()
        };


            $.ajax({
                type: 'POST',
                url: './php/logon_user.php',
                data: formData,
                dataType: 'json',
                encode: true,
                success: function (data) {

                    if (data.success == false) {

                        console.error(data.error);
                        txtAccountUnknown.show();
                        event.preventDefault();
                    }
                    else {

                        frmLogon.unbind().submit();

                    }
                },
                error: function (data) {

                    console.error("Something went wrong!");
                    event.preventDefault();
                }
            });



    });
});