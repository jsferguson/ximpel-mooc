/*
 *  This javascript file is used only by the admin_console.php file. See admin_console.php for details on what the
 *  console is for.
 */
$(function () {


    /*
     * The following are jQuery handles to the HTML-elements for faster access
     */
    var fieldsetUsersTable          = $('#fieldsetUsersTable');
    var legendUsers                 = $('#legendUsers');
    var tabelUsers                  = $('#tabelUsers');
    
    var fieldsetUserHistory         = $('#fieldsetUserHistory');
    var legendHistory               = $('#legendHistory');
    var tabelUserHistory            = $('#tabelUserHistory');
    var spanNoUserHistory           = $('#no_history');
    var spanNoUsers                 = $('#no_users');

    var btnLogout                   = $('#btnLogout');

    var txtAdminName                = $('#txtAdminName');
    var txtAdminpassword            = $('#txtAdminpassword');
    var btnSaveChanges              = $('#btnSaveChanges');
    var cbSubjects                  = $('#cbSubjects');
    var cbLinks                     = $('#cbLinks');
    var cbTags                      = $('#cbTags');
    var divCheckBoxes               = $('#divCheckBoxes');

    // The history data
    var historyData = null;
    // The name of the user, for which the history data has been loaded
    var userHistory = null;
    

    /*
     *  This function loads the user data (name, full name, email address and password) for all XIMPEL users
     *  for the overview table. It uses AJAX to access the PHP-script that reads all the data from the database.
     */
    function getUsersFromDB(){

        var formData = {
            
            'command'       : "GET_ALL_USERS"
        };
        $.ajax({
            type         : 'POST',
            url          : './php/console.php',
            data         : formData,
            dataType     : 'json',
            encode       : true,
            success     : function (userData){

                if (userData.success == false){

                    console.error("Database query failed: " + data.err);
                }
                else {

                    generateUsersTabel(userData.data)
                }
            },
            error       : function (userData){

               console.error("Serious database error: " + JSON.stringify(userData.data));
            }
        });
    }

    /*
     *  This function is called when the table containing the users needs to be generated. It takes an array of
     *  users to populate the table.
     */
    function generateUsersTabel(userData){

        /*
         *  if there are no users in the database, hide the filter-textboxes and show the <span> that shows the message
         *  that there are no users
         */
        if (userData.length == 0){

            spanNoUsers.show();
            fieldsetUsersTable.show();
            divCheckBoxes.hide();

        }
        // Generate the over of users table
        else {

            data = '<table id = "tabelUsers"><tr><td><b>Username</b></td><td><b>Full name</b></td><td><b>Email</b></td><td><b>Password</b></td><td></td><td></td><td></td><td></td></tr>';

            for (var i = 0; i < userData.length; i++ ) {

                data = data + '<tr id = ' + userData[i].user_id + '>' +
                    '<td><input id="txtUsername" type="text" name="txtUsername" size="10" value="'+ userData[i].user_name  +'"></td>' +
                    '<td><input id="txtFullName" type="text" name="txtFullName" size="20" value="' + userData[i].full_name+ '"></td>' +
                    '<td><input id="txtEmail" type="text" name="txtEmail" size="20" value="' + userData[i].email+ '"></td>' +
                    '<td><input id="txtPassword" type="text" name="txtPassword" size="15" value="' + userData[i].password+ '"></td>' +
                    '<td><button class="btnUpdateUser">Update User</button></td>' +
                    '<td><button class="btnDeleteHistory">Delete History</button></td>' +
                    '<td><button class="btnDeleteUser">Delete User</button></td>' +
                    '<td><button class="btnViewHistory">View History</button></td></tr>';
            }

            data = data + '</table>';
            tabelUsers.html(data);
            // hide the message that there are no users
            spanNoUsers.hide();
            // show the containing fieldset of users
            fieldsetUsersTable.show();
            // show the tickbox filters
            divCheckBoxes.show();
        }
    }

    /*
     *  This function retrieves the historical data for a specific user, that is listed in the variable (formData)
     *  that is received from the caller. It uses an AJAX call to POST the request.
     */
    function getUserHistoryFromDB(formData){


        $.ajax({
            type         : 'POST',
            url          : './php/console.php',
            dataType     : 'json',
            data         : formData,
            encode       : true,
            success     : function (userData){

                if (userData.success == false) {

                    console.error("Database query failed: " + data.err);
                }

                /*
                 *  assign the loaded historic data into the historyData variable and assign the user's full name
                 *  to the userHistory variable.
                 *  Call the generateUserHistoryTable function, to create the table containing the user's historic
                 *  data.
                 */
                else {

                    historyData = userData.data;
                    userHistory = formData.full_name;
                    generateUserHistoryTabel(historyData, userHistory);
                }
            },
            error       : function (userData){

                console.error("Serious database error: " + JSON.stringify(userData.data));
            }
        });
    }

    /*
     *  This function generates the table that contains a user's historical data
     *
     */
    function generateUserHistoryTabel(userData, full_name){

        /*
         *  If the returned data is empty, then the user has no history yet. A <span> element indicating this is then
         *  made visible.
         */
        if (userData.length == 0){

            spanNoUserHistory.text("There is no history for " + full_name);
            legendHistory.html("<b>History for " + full_name + "</b>");
            tabelUserHistory.html(null);
            spanNoUserHistory.show();
            fieldsetUserHistory.show();
        }
        else {

            /*
             *  The user has a history and the table is generated.
             */
            spanNoUserHistory.hide();
            legendHistory.html("<b>History for " + full_name + "</b>");
            data = '<table id = "tabelUserHistory"><tr><td><b>Item</b></td><td><b>Description</b></td><td><b>Category</b><td><b>Timestamp</b></td></tr>';

            for (var i = 0; i < userData.length; i++ ) {

                var show = false;

                // if the Show Links checkbox is checked, show this table row
                if (userData[i].category == "link" && cbLinks.is(':checked')){

                    show = true;
                }
                // if the Show Subjects checkbox is checked, so this table row

                else if (userData[i].category == "subject" && cbSubjects.is(':checked')){

                    show = true;
                }
                // if the Show Tags checkbox is checked, so this table row
                else if (userData[i].category == "tag" && cbTags.is(':checked')){

                    show = true;
                }

                if(show){

                    // translate the Unix epoch timestamp into a user-friendly format
                    var d = new Date();
                    date = new Date(userData[i].time_stamp*1000 + d.getTimezoneOffset() * 60000);

                    data = data + '<tr class="'+userData[i].category+'">' +
                        '<td>'+ userData[i].item  +'</td>' +
                        '<td>' + userData[i].description + '</td>' +
                        '<td>' + userData[i].category + '</td>' +
                        '<td>' + date + '</td></tr>';
                }

            }
            data = data + '</table>';
            tabelUserHistory.html(data);
            fieldsetUserHistory.show();
        }
    }

    /*
     *  register a handler for when the user selects the view History for a certain user
     */
    tabelUsers.on('click', '.btnViewHistory', function(){

        var user_id = $(this).closest('tr').attr('id');
        var full_name = $(this).closest('tr').find("#txtFullName").val();
        var formData = {

            'command'    : "GET_USER_HISTORY",
            'user_id'       : user_id,
            'full_name'     : full_name
        };

        // call the function that loads the chosen user's history and pass the user information to this function
        getUserHistoryFromDB(formData);
    });

    /*
     *  register a handler for when the user clicks on the Delete History button for a certain user
     */
    tabelUsers.on('click', '.btnDeleteHistory', function(){

        var user_id = $(this).closest('tr').attr('id');

        var formData = {

            'command'    : "DELETE_USER_HISTORY",
            'user_id'     : user_id
        };

        if(confirm("Are you sure you want to delete the user's history?")){

            // call the function that deletes the history for a selected user and pass the user's ID to this function
            deleteUserHistoryFromDB(formData);
        }

    });

    /*
     *  This function calls a PHP-script, using AJAX, to delete a given user's history from the database
     */
    function deleteUserHistoryFromDB(formData){


        $.ajax({
            type         : 'POST',
            url          : './php/console.php',
            dataType     : 'json',
            data         : formData,
            encode       : true,
            success     : function (userData){

                if (userData.success == false){

                    console.error("Database query failed: " + data.err);
                }
                else {


                    tabelUserHistory.html(null);
                    fieldsetUserHistory.hide();
                }
            },
            error       : function (userData){

                console.error("Serious database error: " + JSON.stringify(userData.data));
            }
        });

    }

    /*
     *  register a handler for when the user click on the Delete User button for a certain user.
     */
    tabelUsers.on('click', '.btnDeleteUser', function(){

        var user_id = $(this).closest('tr').attr('id');

        var formData = {

            'command'    : "DELETE_USER",
            'user_id'     : user_id

        };

        if(confirm("Are you sure you want to delete the user?")){

            // call the function that deletes the user from the database
            deleteUserFromDB(formData);
        }


    });

    /*
     *  This function calls a PHP-script, using AJAX, to delete a user from the database
     */
    function deleteUserFromDB(formData){

        $.ajax({
            type         : 'POST',
            url          : './php/console.php',
            dataType     : 'json',
            data         : formData,
            encode       : true,
            success     : function (userData){

                if (userData.success == false){

                    console.error("Database query failed: " + data.err);
                }
                else {

                    //clear the tables and reload them again
                    tabelUserHistory.html(null);
                    tabelUsers.html(null);
                    fieldsetUserHistory.hide();
                    getUsersFromDB();
                }
            },
            error       : function (userData){

                console.error("Serious database error: " + JSON.stringify(userData.err));
            }
        });
    }

    /*
     *  register a handler for when a user has clicked the Update User button. This is done when
     *  the administrator makes one or more changes to a user.
     */
    tabelUsers.on('click', '.btnUpdateUser', function(){

        var user_id = $(this).closest('tr').attr('id');
        var user_name = $(this).closest('tr').find("#txtUsername").val();
        var full_name = $(this).closest('tr').find("#txtFullName").val();
        var email = $(this).closest('tr').find("#txtEmail").val();
        var password = $(this).closest('tr').find("#txtPassword").val();
        // read the data from the HTML-elements, containing the updated user data.
        var formData = {

            'command'       : "UPDATE_USER_CREDENTIALS",
            'user_id'       : user_id,
            'user_name'     : user_name,
            'full_name'     : full_name,
            'email'         : email,
            'password'      : password
        };

        // call the function that updates the user and pass the changed data to this users
        updateUserInDB(formData);
    });

    /*
     * This function is calls a PHP-script, using AJAX, to update a user's data in the database
     */
    function updateUserInDB(formData){

        $.ajax({
            type         : 'POST',
            url          : './php/console.php',
            dataType     : 'json',
            data         : formData,
            encode       : true,
            success     : function (userData){

                if (userData.success == false){

                    console.error("Database query failed: " + data.err);
                }
                else {

                    // call the function that reloads the users from the database and rebuild the user overview table
                    getUsersFromDB();
                }
            },
            error       : function (userData){

                console.error("Serious database error: " + JSON.stringify(userData.err));
            }
        });
    }

    /*
     *  This function load the administrator's detail fro the database
     */
    function getAdminFromDB(){

        var formData = {

            'command'       : "GET_ADMIN_DETAILS"

        };

        $.ajax({
            type         : 'POST',
            url          : './php/console.php',
            data         : formData,
            dataType     : 'json',
            encode       : true,
            success     : function (userData){

                if (userData.success == false){

                    console.error("Database query failed: " + data.err);
                }
                else {

                    setAdminDetails(userData.data)
                }
            },
            error       : function (userData){

                console.error("Serious database error: " + JSON.stringify(userData.data));
            }
        });
    }

    /*
     *  This function sets the HTML-elements that contains the administrator's details (name and password)
     */
    function setAdminDetails(data){

        txtAdminName.val(data.name);
        txtAdminpassword.val(data.password);
    }

    // if a change is made to the administrator's name, enable the Update button
    txtAdminName.on('input', function () {

        btnSaveChanges.prop( "disabled", false );
    });

    // if a change is made to the administrator's password, enable the Update button

    txtAdminpassword.on('input', function () {

        btnSaveChanges.prop( "disabled", false );
    });

    /*
     *  Register a handler for when the user clicks on the log out button. It redirects the browser to the logon page
     *
     */
    btnLogout.click(function () {

        console.log("You clicked logoff");
        window.location.href = "./php/logout_admin.php";
    });

    /*
     *  Register a handler for when the users clicks on the Update button, for the administrator's details
     *  (name and password)
     */
    btnSaveChanges.click(function () {
        
        formData = {

            'command'       : "UPDATE_ADMIN_CREDENTIALS",
            'name'          : txtAdminName.val(),
            'password'      : txtAdminpassword.val()
            
        };
        // save the changes to the database
        updateAdminInDB(formData);
    });

    /*
     *  This function calls a PHP-script, using AJAX, and posts the changed administrator details
     */
    function updateAdminInDB(formData){

        $.ajax({
            type         : 'POST',
            url          : './php/console.php',
            dataType     : 'json',
            data         : formData,
            encode       : true,
            success     : function (userData){

                if (userData.success == false){

                    console.error("Updating administrator failed: " + data.err);
                }
                else {

                    btnSaveChanges.prop( "disabled", true );
                }
            },
            error       : function (userData){

                console.error("Serious database error: " + JSON.stringify(userData.err));
            }
        });
    }

    /*
     *  this function calls a PHP-script, using AJAX, which will in turn destroy the current sessionID
     */
    function registerUnload(){

        window.onbeforeunload = function() {



            $.ajax({
                type         : 'POST',
                url          : './php/logout_admin.php',
                dataType     : 'json',
                encode       : true

            });

        };
        window.onunload = function() {



            $.ajax({
                type         : 'POST',
                url          : './php/logout_admin.php',
                dataType     : 'json',
                encode       : true

            });


        };
        
    }

    registerUnload();
    getUsersFromDB();
    getAdminFromDB();

    //reload the user's history, when the ShowLinks filter is changed
    cbLinks.click(function () {

        generateUserHistoryTabel(historyData, userHistory);
    });

    //reload the user's history, when the ShowSubject filter is changes
    cbSubjects.click(function () {

        generateUserHistoryTabel(historyData, userHistory);
    });

    //reload the user's history, when the ShowTags filter is changes
    cbTags.click(function () {

        generateUserHistoryTabel(historyData, userHistory);
    });
});

