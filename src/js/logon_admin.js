/*
 *  This is the script for the administrator logon.
 *  It will first validate the given credentials. If these are not correct, the user will see a warning and will
 *  remain at this page.
 *  If the credentials are valie, the user will be redirected to the administrator console.
 */
$(function(){

    var user_name           = $('#user_name');
    var password            = $('#password');
    var frmLogon            = $('#frmLogon');
    var txtAccountUnknown   = $('#txtAccountUnknown');
    var btnSubmit           = $('#btnSubmit');

    frmLogon.submit(function(event) {

        if(!frmLogon.valid){

            event.preventDefault();
        }

        var formData = {

            "user_name"         : user_name.val(),
            "password"          : password.val()
        };

        $.ajax({

            type: 'POST',
            url: './admin_login.php',
            data: formData,
            dataType: 'json',
            encode: true,
            success: function (data) {

               
                if (data.success == false) {

                    console.error(data.error);
                    txtAccountUnknown.show();
                    event.preventDefault();
                }
                else {

                    console.log("submitting");
                    frmLogon.unbind().submit();

                }
            },
            error: function (data) {

                console.error(JSON.stringify(data));
                event.preventDefault();
            }
        });
    });
});