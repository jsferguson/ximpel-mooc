/*
 *  This is the script for the new user page.
 *  Some of the form validation is performed using HTML5 functionality. For instance, a username may only
 *  contain letters and numbers and it may not contain any spaces. Also, the script will check if the username
 *  is not already taken by another user first.
 *  Also, the script will verify if the chosen password is the same as the re-type password element.
 *  If the given credientials are valid, the data is passed to a PHP-script that will create the account.
 *  Then the user is redirected to the logon screen, where the user can logon with the new account.
 */
$(function(){

    var username        = $('#user_name');
    var fullName        = $('#txtFullName');
    var emailAddress    = $('#txtEmail');
    var pwd1            = $('#password');
    var pwd2            = $('#txtPwd2');
    var frmNewAccount   = $('#frmNewAccount');
    var spanPwdMismatch = $('#txtPwdMismatch');
    var btnSubmitNewUser= $('#btnSubmitNewUser');
    var txtNameTaken    = $('#txtNameTaken');

    frmNewAccount.submit(function(event) {

        if(pwd1.val() != pwd2.val()){

            spanPwdMismatch.show();
            event.preventDefault();
        }
        else {

            event.preventDefault();
            var formData = {
                
                "user_name"         : username.val(),
                "full_name"         : fullName.val(),
                "password"          : pwd1.val(),
                "email"             : emailAddress.val()
            };

            

            $.ajax({
                type            : 'POST',
                url             : './php/insert_new_user.php',
                data            : formData,
                dataType        : 'json',
                encode          : true,
                success         : function (data) {

                    if (data.success == false){

                        console.error(data.error);
                        txtNameTaken.show();
                    }
                    else {

                        frmNewAccount.unbind().submit();
                    }
                },
                error           : function (data){

                    console.error("Something went wrong!");
                }
            });

        }

    });


});