--
-- File generated with SQLiteStudio v3.0.7 on Tue Jun 14 12:40:03 2016
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Users
DROP TABLE IF EXISTS Users;

CREATE TABLE Users (
    user_id   INTEGER PRIMARY KEY AUTOINCREMENT
                      UNIQUE
                      NOT NULL,
    user_name TEXT    UNIQUE ON CONFLICT FAIL
                      NOT NULL,
    full_name TEXT    NOT NULL,
    email     TEXT    NOT NULL,
    password  TEXT    NOT NULL ON CONFLICT FAIL
);


-- Table: VisitedLinks
DROP TABLE IF EXISTS VisitedLinks;

CREATE TABLE VisitedLinks (
    visited_id  INTEGER PRIMARY KEY AUTOINCREMENT
                        UNIQUE
                        NOT NULL,
    user_id     INTEGER REFERENCES Users (user_id) ON DELETE CASCADE
                        NOT NULL ON CONFLICT FAIL,
    url         TEXT    NOT NULL ON CONFLICT FAIL,
    description TEXT    NOT NULL ON CONFLICT FAIL,
    time_stamp  INTEGER NOT NULL ON CONFLICT FAIL
);


-- Table: History
DROP TABLE IF EXISTS History;

CREATE TABLE History (
    id          INTEGER PRIMARY KEY AUTOINCREMENT
                        UNIQUE
                        NOT NULL,
    user_id     INTEGER NOT NULL
                        REFERENCES Users (user_id) ON DELETE CASCADE,
    item        TEXT    NOT NULL,
    description TEXT,
    time_stamp  INTEGER,
    category    TEXT
);


-- Table: AdminSessions
DROP TABLE IF EXISTS AdminSessions;

CREATE TABLE AdminSessions (
    admin_id   INTEGER REFERENCES AdminUsers (id) ON DELETE CASCADE,
    session_id TEXT    NOT NULL,
    timestamp  TEXT    NOT NULL
);


-- Table: AdminUsers
DROP TABLE IF EXISTS AdminUsers;

CREATE TABLE AdminUsers (
    id       INTEGER PRIMARY KEY AUTOINCREMENT
                     UNIQUE
                     NOT NULL,
    name     TEXT    NOT NULL
                     UNIQUE,
    password TEXT    NOT NULL
);

INSERT INTO AdminUsers (
                           id,
                           name,
                           password
                       )
                       VALUES (
                           1,
                           'admin',
                           'boss'
                       );


-- Table: Tags
DROP TABLE IF EXISTS Tags;

CREATE TABLE Tags (
    tag_id     INTEGER PRIMARY KEY AUTOINCREMENT
                       NOT NULL
                       UNIQUE,
    user_id    INTEGER NOT NULL ON CONFLICT FAIL
                       REFERENCES Users (user_id) ON DELETE CASCADE,
    tag_name   TEXT    NOT NULL ON CONFLICT FAIL,
    tag_value  INTEGER NOT NULL ON CONFLICT FAIL,
    time_stamp INTEGER
);


-- Table: VisitedSubjects
DROP TABLE IF EXISTS VisitedSubjects;

CREATE TABLE VisitedSubjects (
    visited_id  INTEGER PRIMARY KEY AUTOINCREMENT
                        NOT NULL
                        UNIQUE,
    subject_id  TEXT    NOT NULL,
    user_id     INTEGER NOT NULL
                        REFERENCES Users (user_id) ON DELETE CASCADE,
    time_stamp  INTEGER NOT NULL,
    description TEXT
);


-- Table: UserSessions
DROP TABLE IF EXISTS UserSessions;

CREATE TABLE UserSessions (
    user_id    INTEGER REFERENCES Users (user_id) ON DELETE CASCADE,
    session_id TEXT,
    timestamp  INTEGER
);


-- Trigger: triggerClearPreviousSessions
DROP TRIGGER IF EXISTS triggerClearPreviousSessions;
CREATE TRIGGER triggerClearPreviousSessions
        BEFORE INSERT
            ON AdminSessions
BEGIN
    DELETE FROM AdminSessions;
END;


-- Trigger: triggerVisitedSubject
DROP TRIGGER IF EXISTS triggerVisitedSubject;
CREATE TRIGGER triggerVisitedSubject
        BEFORE INSERT
            ON VisitedSubjects
      FOR EACH ROW
BEGIN
    INSERT INTO History (
                            user_id,
                            item,
                            description,
                            time_stamp,
                            category
                        )
                        VALUES (
                            new.user_id,
                            new.subject_id,
                            new.description,
                            new.time_stamp,
                            subject
                        );
END;


-- Trigger: triggerVisitedLink
DROP TRIGGER IF EXISTS triggerVisitedLink;
CREATE TRIGGER triggerVisitedLink
         AFTER INSERT
            ON VisitedLinks
      FOR EACH ROW
BEGIN
    INSERT INTO History (
                            user_id,
                            item,
                            description,
                            time_stamp,
                            category
                        )
                        VALUES (
                            new.user_id,
                            new.url,
                            new.description,
                            new.time_stamp,
                            link
                        );
END;


-- Trigger: triggerInsertTags
DROP TRIGGER IF EXISTS triggerInsertTags;
CREATE TRIGGER triggerInsertTags
         AFTER INSERT
            ON Tags
BEGIN
    INSERT INTO History (
                            user_id,
                            item,
                            description,
                            time_stamp,
                            category
                        )
                        VALUES (
                            new.user_id,
                            "Tag update",
                            "TAG: " || new.tag_name || " = " || new.tag_value,
                            new.time_stamp,
                            tag
                        );
END;


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
