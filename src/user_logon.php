<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ximpel Demo</title>
    <link rel="stylesheet" href="./css/user_logon.css" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="./js/logon.js"></script>
</head>
<body>

<fieldset>
    <legend>Logon with your XIMPEL account</legend>
    <form id="frmLogon" method="post" action="ximpel.php">
        <span id="txtAccountUnknown" hidden>Username password combination unknown!</span><br>
        <label for="user_name">Your username</label><br>
        <input id="user_name" name="user_name" required autofocus><br><br>
        <label for="password">Your password</label><br>
        <input id="password" type="password" name="password" required><br>
        <input id="btnSubmit" type="submit" value="Logon">
    </form>
</fieldset>
<a href="new_user.php">New user?</a>
</body>
</html>