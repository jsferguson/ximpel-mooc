<?php
/*
 *  This script will check if the current sessionID matches an entry in the AdminSessions table. If there is no
 *  match, a false is returned.
 */


function checkSessionID(){

    session_start();
    $session_id = session_id();

    try {

        $file_db = getDB();
        $query = "select admin_id from AdminSessions where session_id = :session_id;";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':session_id', $session_id);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;
        if ($data ){

            return true;
        }
        else {

            return false;
        }

    }
    catch (Exception $e){

        return false;
    }

}
