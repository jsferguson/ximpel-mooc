<?php
/*
 *  This script destroys the sessionID for the administrator that is currently logged on. This is done
 *  by deleting all sessions that have the current sessionsID.
 */

include('get_db.php');

function destroySessionID($session_id){

    session_destroy();
    try {

        $file_db = getDB();
        $query = "delete from AdminSessions where session_id = :session_id;";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':session_id', $session_id);
        $stmt->execute();
        $file_db = null;
    }
    catch (Exception $e){

    }
}

session_start();
destroySessionID(session_id());
session_destroy();
header('Location: ../admin_login.php');