<?php
/*
 *  This function tries to find a userID for the current sessionID. If it does not return anything, then it returns
 *  false
 */
function checkSessionID(){

    session_start();
    $session_id = session_id();

    try {

        $file_db = getDB();
        $query = "select user_id from UserSessions where session_id = :session_id;";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':session_id', $session_id);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;
        if ($data ){

            return true;
        }
        else {

            return false;
        }

    }
    catch (Exception $e){

        return false;
    }

}
