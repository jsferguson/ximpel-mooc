<?php


function getDB(){

    $file_db = new PDO('sqlite:../db/ximpel.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $file_db;
}

function checkSessionID(){

    session_start();
    $session_id = session_id();

    try {

        $file_db = getDB();
        $query = "select admin_id from AdminSessions where session_id = :session_id;";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':session_id', $session_id);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;
        if ($data ){

            return true;
        }
        else {

            return false;
        }

    }
    catch (Exception $e){

        return false;
    }

}

function deleteUser($_in_user_id){

    $output = array();
    $commands = array(
        "delete from Tags where user_id = :user_id;",
        "delete from VisitedSubjects where user_id = :user_id;",
        "delete from History where user_id = :user_id;",
        "delete from UserSessions where user_id = :user_id;",
        "delete from VisitedLinks where user_id = :user_id;",
        "delete from Users where user_id = :user_id;"

    );
    try {

        $file_db = getDB();

        foreach ($commands as $command){

            $stmt = $file_db->query($command);
            $stmt->bindParam(':user_id', $_in_user_id);
            $stmt->execute();
        }

        $file_db = null;
        $output['success'] = true;

        return $output;
    }
    catch (PDOException $e) {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = $e->getMessage();

        return $output;
    }
}

function removeUser(){

    if (checkSessionID()){

        echo json_encode(deleteUser($_POST['user_id']));
    }
    else {

        $output = array();
        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = "Administrator is not logged on!";
        echo json_encode($output);
    }
}

function deleteUserHistory($_in_user_id){

    $output = array();
    $commands = array(
        "delete from History where user_id = :user_id",
        "delete from Tags where user_id = :user_id",
        "delete from VisitedLinks where user_id = :user_id",
        "delete from VisitedSubjects where user_id = :user_id;"
    );

    try {

        $file_db = getDB();

        foreach ($commands as $command){

            $stmt = $file_db->query($command);
            $stmt->bindParam(':user_id', $_in_user_id);
            $stmt->execute();

        }

        $file_db = null;
        $output['success'] = true;

        return $output;
    }
    catch (PDOException $e) {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = $e->getMessage();

        return $output;
    }
}

function removeUserHistory(){

    if (checkSessionID()){

        echo json_encode(deleteUserHistory($_POST['user_id']));
    }
    else {

        $output = array();
        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = "Administrator is not logged on!";
        echo json_encode($output);

    }
}

function getAdmin(){

    $output = array();
    try {


        $file_db = getDB();
        $stmt = $file_db->query("select id, name, password from AdminUsers;");

        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;
        $output['success'] = true;
        $output['data'] = $data;

        return $output;
    }
    catch (PDOException $e) {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = $e->getMessage();

        return $output;
    }
}

function getAdminDetails(){

    if (checkSessionID()){

        echo json_encode(getAdmin());
    }
    else {

        $output = array();
        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = "Administrator is not logged on!";
        echo json_encode($output);

    }
}

function getUsers(){

    $output = array();
    try {


        $file_db = getDB();
        $stmt = $file_db->query("select user_id, user_name, full_name, email, password from Users order by user_name;");

        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $file_db = null;
        $output['success'] = true;
        $output['data'] = $data;

        return $output;
    }
    catch (PDOException $e) {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = $e->getMessage();

        return $output;
    }
}

function getAllUsers(){

    if (checkSessionID()){

        echo json_encode(getUsers());
    }
    else {

        $output = array();
        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = "Administrator is not logged on!";
        echo json_encode($output);

    }
}

function getUserHistory($_in_user_id){

    $output = array();
    try {

        $file_db = getDB();
        $stmt = $file_db->query("select item, description, time_stamp, category from History where user_id = :user_id order by time_stamp;");
        $stmt->bindParam(':user_id', $_in_user_id);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $file_db = null;
        $output['success'] = true;
        $output['data'] = $data;

        echo json_encode($output);
    }
    catch (PDOException $e) {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = $e->getMessage();

        echo json_encode($output);
    }
}

function updateAdmin($_in_name, $_in_password) {

    session_start();
    $session_id = session_id();

    try {

        $file_db = getDB();
        $insert = "update AdminUsers set name = :name, password = :password where id = (select admin_id from AdminSessions where session_id = :session_id)";
        $stmt = $file_db->prepare($insert);
        $stmt->bindParam(':session_id', $session_id);
        $stmt->bindParam(':name', $_in_name);
        $stmt->bindParam(':password', $_in_password);
        $stmt->execute();
        $file_db = null;
        $data['success'] = true;
        $data['message'] = 'Succes!';

        return $data;
    }


    catch (PDOException $e) {

        $data['success'] = false;
        $data['message'] = 'Failed!';
        $data['err'] = $e->getMessage();;

        return $data;
    }
}

function updateAdminCredentials(){

    if (checkSessionID()){

        $response =  updateAdmin($_POST['name'], $_POST['password']);
        echo json_encode($response);
    }
    else {

        $output = array();
        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = "Administrator is not logged on!";

        echo json_encode($output);
    }
}

function updateUser($_in_user_id, $_in_username, $_in_password, $_in_full_name, $_in_email) {

    try {

        $file_db = getDB();
        $insert = "update Users set user_name = :user_name, full_name = :full_name, email = :email, password = :password where user_id = :user_id;";
        $stmt = $file_db->prepare($insert);
        $stmt->bindParam(':user_id', $_in_user_id);
        $stmt->bindParam(':user_name', $_in_username);
        $stmt->bindParam(':full_name', $_in_full_name);
        $stmt->bindParam(':email', $_in_email);
        $stmt->bindParam(':password', $_in_password);
        $stmt->execute();
        $file_db = null;
        $data['success'] = true;
        $data['message'] = 'Succes!';

        echo json_encode($data);
    }


    catch (PDOException $e) {

        $data['success'] = false;
        $data['message'] = 'Failed!';
        $data['err'] = $e->getMessage();;

        echo json_encode($data);
    }
}

function updateUserCredentials(){

    if (checkSessionID()){

        updateUser($_POST['user_id'], $_POST['user_name'], $_POST['password'], $_POST['full_name'], $_POST['email']);
    }
    else {

        $output = array();
        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = "Administrator is not logged on!";

        echo json_encode($output);

    }
}


function getCommand(){

    if ($_POST['command']){

        $command = $_POST['command'];
        switch ($command) {

            case "DELETE_USER"://done
                removeUser();
                break;
            case "DELETE_USER_HISTORY":
                removeUserHistory();
                break;
            case "GET_ADMIN_DETAILS":
                getAdminDetails();
                break;
            case "GET_ALL_USERS":
                getAllUsers();
                break;
            case "GET_USER_HISTORY":
                getUserHistory($_POST['user_id']);
                break;
            case "UPDATE_ADMIN_CREDENTIALS":
                updateAdminCredentials();
                break;
            case "UPDATE_USER_CREDENTIALS":
                updateUserCredentials();
                break;
            default:
                echo "Unknown command given";
        }
    }
    else {

        echo "No command received";
    }
}

getCommand();