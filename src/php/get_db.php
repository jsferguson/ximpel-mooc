<?php
/**
 * Created by PhpStorm.
 * User: jsferg
 * Date: 27/12/14
 * Time: 08:50
 */

function getDB(){

    $file_db = new PDO('sqlite:../db/ximpel.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $file_db;
}

