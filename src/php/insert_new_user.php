<?php
/*
 *  This script inserts a new user into the database. If the supplied username already exists in the database,
 *  the script will only return a warning and not insert the new user.
 */

include('get_db.php');

function checkIfUserNameIsFree($_in_username){

    $free = false;
    try {

        $file_db = getDB();
        $query = "select user_id from Users where user_name = :user_name;";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':user_name', $_in_username);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;
        if (! $data ){

            $free = true;
        }
    }
    catch (Exception $e){

        return false;
    }

    return $free;
}

function insertNewUser($_in_username, $_in_full_name, $_in_password, $_in_email) {
    
    try {

        $file_db = getDB();
        $insert = "insert into Users (user_name, full_name, email, password) values (:user_name, :full_name, :email, :password);";
        $stmt = $file_db->prepare($insert);
        $stmt->bindParam(':user_name', $_in_username);
        $stmt->bindParam(':full_name', $_in_full_name);
        $stmt->bindParam(':email', $_in_email);
        $stmt->bindParam(':password', $_in_password);
        $stmt->execute();

        return true;

    }
    catch (Exception $e) {

        return false;
    }
}

$output = array();

if (checkIfUserNameIsFree($_POST['user_name'])){

   if (insertNewUser($_POST['user_name'], $_POST['full_name'], $_POST['password'], $_POST['email'])){

       $output['success'] = true;
       $output['error'] = "User has been created";
   }
   else {

       $output['success'] = false;
       $output['error'] = "Unknown error while inserting user!";

   }
}
else {

    $output['success'] = false;
    $output['error'] = "User name is already taken";
}

echo json_encode($output);


