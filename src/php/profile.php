<?php

function getDB(){

    $file_db = new PDO('sqlite:../db/ximpel.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $file_db;
}

function getUserId(){

    session_start();
    $session_id = session_id();
    $output = array();
    try {

        $file_db = getDB();
        $query = "select user_id from UserSessions where session_id = :session_id;";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':session_id', $session_id);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;

        if ($data ){

            $output['success'] = true;
            $output['data'] = $data;
            return $output;
        }
        else {

            $output['success'] = false;

            return $output;
        }

    }
    catch (Exception $e){

        $output['success'] = false;
        $output['error'] = $e->getMessage();

        return $output;
    }
}

function getCommand(){

    if ($_POST['command']){

        $command = $_POST['command'];
        switch ($command) {

            case "INSERT_VISITED_SUBJECT"://done
                insertVisitedSubject($_POST['subjectID'], $_POST['description']);
                break;
            case "INSERT_VISITED_LINK":
                insertVisitedLink($_POST['url'], $_POST['description']);
                break;
            case "INSERT_USER_TAGS"://done
                insertUserTags($_POST['tags']);
                break;
            case "GET_USER_HISTORY"://done
                getUserHistory();
                break;
            case "GET_LAST_VISITED"://done
                getLastVisited();
                break;
            case "CLOSE_USER_SESSION"://done
                deleteSession();
                break;
            default:
                echo "Unknown command given";
        }
    }
    else {

        echo "No command received";
    }
}

function insertVisitedSubject($_in_subjectID, $_in_description) {

    $output = array();
    $data = getUserId();
    $user_id = null;
    if ($data['success']){

        $user_id = $data['data']['user_id'];
    }
    else {

        $output['success'] = false;
        $output['message'] = 'Failed!';

        echo json_encode($output);
    }
    try {

        $file_db = getDB();
        $insert = "insert into VisitedSubjects (subject_id, user_id, time_stamp, description) values (:subjectID, :user_id, :time_stamp, :description);";

        $stmt = $file_db->prepare($insert);
        $date = date_create();
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':subjectID', $_in_subjectID);
        $stmt->bindParam(':description', $_in_description);
        $stmt->bindParam(':time_stamp', date_timestamp_get($date));
        $stmt->execute();
        $file_db = null;
        $data['success'] = true;
        $data['message'] = 'Succes!';

        echo json_encode($data);
    }
    catch (PDOException $e) {

        $data['success'] = false;
        $data['message'] = 'Failed!';
        $data['err'] = $e->getMessage();

        echo json_encode($data);
    }
}
function insertVisitedLink($_in_url, $_in_description) {

    $output = array();
    $data = getUserId();
    $user_id = null;
    if ($data['success']){

        $user_id = $data['data']['user_id'];
    }
    else {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        echo json_encode($output);
    }


    try {

        $file_db = getDB();
        $insert = "insert into VisitedLinks (user_id, url, description, time_stamp) values (:user_id, :url, :description, :time_stamp);";
        $stmt = $file_db->prepare($insert);
        $date = date_create();
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':url', $_in_url);
        $stmt->bindParam(':description', $_in_description);
        $stmt->bindParam(':time_stamp', date_timestamp_get($date));
        $stmt->execute();
        $file_db = null;
        $data['success'] = true;
        $data['message'] = 'Succes!';

        echo json_encode($data);
    }

    catch (PDOException $e) {

        $data['success'] = false;
        $data['message'] = 'Failed!';
        $data['err'] = $e->getMessage();;

        echo json_encode($data);
    }
}
function insertUserTags($_in_tags) {

    $output = array();
    $data = getUserId();
    $user_id = null;
    if ($data['success']){

        $user_id = $data['data']['user_id'];
    }
    else {

        $output['success'] = false;
        $output['message'] = 'Failed!';

        echo json_encode($output);
    }
    try {

        $file_db = getDB();
        $insert = "insert or replace into Tags (tag_id, user_id, tag_name, tag_value, time_stamp) 
                  values ((select tag_id from Tags where user_id = :user_id and tag_name = :tag_name), :user_id ,:tag_name, :tag_value, :time_stamp);";

        $stmt = $file_db->prepare($insert);
        $date = date_create();
        foreach ($_in_tags as $tag){

            $stmt->bindParam(':tag_name', $tag['tagName']);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':tag_value', $tag['tagValue']);
            $stmt->bindParam(':time_stamp', date_timestamp_get($date));
            $stmt->execute();
        }
        $data['success'] = true;
        $data['message'] = 'Worked!';

        echo json_encode($data);

    }
    catch (Exception $e) {

        $data['success'] = false;
        $data['message'] = 'Failed!';
        $data['err'] = $e->getMessage();;

        echo json_encode($data);
    }
}
function getVisited(){

    $output = array();
    $data = getUserId();
    $user_id = null;

    if ($data['success']){

        $user_id = $data['data']['user_id'];
    }
    else {

        $output['success'] = false;
        $output['message'] = 'Failed!';

        return $output;
    }
    try {

        $file_db = getDB();
        $stmt = $file_db->query("select subject_id from VisitedSubjects where user_id = :user_id order by time_stamp;");
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $file_db = null;
        $output['success'] = true;
        $output['message'] = 'Worked!';
        $output['visited'] = $data;

        return $output;
    }
    catch (PDOException $e) {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = $e->getMessage();

        return $output;
    }
}
function getTags(){

    $output = array();
    $data = getUserId();
    $user_id = null;

    if ($data['success']){

        $user_id = $data['data']['user_id'];
    }
    else {

        $output['success'] = false;
        $output['message'] = 'Failed!';

        return $output;
    }
    try {


        $file_db = getDB();
        $stmt = $file_db->query("select tag_name, tag_value from Tags where user_id = :user_id order by time_stamp, tag_name;");
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $file_db = null;
        $output['success'] = true;
        $output['message'] = 'Worked!';
        $output['tags'] = $data;

        return $output;
    }
    catch (PDOException $e) {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = $e->getMessage();;

        return $output;
    }
}
function getUserHistory(){

    $output = array();
    $visited = getVisited();
    $tags = getTags();

    if (($visited['success'] == true) && ($tags['success'] == true)) {

        $output['success'] = true;
        $output['message'] = "Worked!";
        $output['tags'] = $tags['tags'];
        $output['visited'] = $visited['visited'];
    }
    else {

        $output['success'] = false;
        $output['message'] = "Failed!";
        $output['err'] = $visited['err'] . ' ' . $tags['err'];
    }

    echo json_encode($output);
}
function getLastVisited(){

    $output = array();
    $data = getUserId();
    $user_id = null;

    if ($data['success']){

        $user_id = $data['data']['user_id'];
    }
    else {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        echo json_encode($output);
    }
    try {

        $file_db = getDB();
        $stmt = $file_db->query("select subject_id from VisitedSubjects where user_id = :user_id order by time_stamp DESC;");
        $stmt->bindParam(':user_id', $user_id);

        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;
        $output['success'] = true;
        $output['message'] = 'Worked!';
        $output['last_visited'] = $data;

        echo json_encode($output);
    }
    catch (PDOException $e) {

        $output['success'] = false;
        $output['message'] = 'Failed!';
        $output['err'] = $e->getMessage();

        echo json_encode($output);
    }
}
function deleteSession() {

    /*
     *  Start a new session and assign the current sessionID to a variable
     */
    session_start();
    $session_id = session_id();

    try {

        $file_db = getDB();
        $insert = "delete from UserSessions where session_id = :session_id";


        $stmt = $file_db->prepare($insert);
        $stmt->bindParam(':session_id', $session_id);
        $stmt->execute();
        session_destroy();
    }
    catch (Exception $e) {

        $data['success'] = false;
        $data['message'] = 'Failed!';
        $data['err'] = $e->getMessage();;

        echo json_encode($data);
    }
}

getCommand();