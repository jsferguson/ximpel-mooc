<?php
/*
 *  This script will logon a user and create a sessionID which is also stored in the database, under
 *  UserSessions. The user is validated by checking if the username and password have a match in the database.
 *  If a match is not found, an error is returned.
 */

include('get_db.php');

function checkUser($_in_username, $_in_password, $_in_output){

    try {

        $file_db = getDB();
        $query = "select user_id from Users where user_name = :user_name and password = :password;";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':user_name', $_in_username);
        $stmt->bindParam(':password', $_in_password);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;
        if ($data ){

            $_in_output['success'] = true;
            $_in_output['id'] = $data['user_id'];
        }
        else {

            $_in_output['success'] = false;
            $_in_output['error'] = "Combination not found";
        }
    }
    catch (Exception $e){

        $_in_output['success'] = false;
        $_in_output['error'] = $e->getMessage();
    }

    return $_in_output;
}

function createUserSession($_in_username, $_in_password){

    session_start();
    session_regenerate_id();
    $sessionID = session_id();
    try {

        $date = date_create();
        $file_db = getDB();
        $query = "insert into UserSessions (user_id, session_id, timestamp) values ((select user_id from Users where user_name = :user_name and password = :password), :session_id, :timestamp);";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':timestamp', date_timestamp_get($date));
        $stmt->bindParam(':user_name', $_in_username);
        $stmt->bindParam(':password', $_in_password);
        $stmt->bindParam(':session_id', $sessionID);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $file_db = null;
        if ($data ){

            $_in_output['success'] = true;
        }
        else {

            $_in_output['success'] = false;
            $_in_output['error'] = "Combination not found";
        }
    }
    catch (Exception $e){

        $_in_output['success'] = false;
        $_in_output['error'] = $e->getMessage();
    }

    return $_in_output;
}

function removeOldSessions($_in_user_id){

    try {

        $file_db = getDB();
        $query = "delete from UserSessions where user_id = :user_id;";
        $stmt = $file_db->prepare($query);
        $stmt->bindParam(':user_id', $_in_user_id);
        $stmt->execute();
        $file_db = null;
    }
    catch (Exception $e){

    }
}


$output = checkUser($_POST['user_name'], $_POST['password']);
if ($output['success'] == true){

    $user_id = $output['id'];
    removeOldSessions($user_id);
    createUserSession($_POST['user_name'], $_POST['password']);
}

echo json_encode($output);
